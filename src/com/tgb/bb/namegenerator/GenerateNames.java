/*
 * Created on 5 Feb. 11
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tgb.bb.namegenerator.utilities.IRenderer;
import com.tgb.bb.namegenerator.utilities.Informer;
import com.tgb.bb.namegenerator.utilities.renderer.RendererFile;
import com.tgb.bb.namegenerator.utilities.renderer.RendererFileRandom;
import com.tgb.bb.namegenerator.utilities.renderer.RendererFileRandomOneNamePerLine;
import com.tgb.bb.namegenerator.utilities.renderer.RendererHTML;

public class GenerateNames implements Runnable
{
	private static Logger logger = LoggerFactory.getLogger(GenerateSyllables.class);
	
	static int iPart = 1;
	static int iNbOfGeneratedNames = 0;
	
	static boolean endAsked = false;
	private static int minimumNumberOfLetters = 4;
	private static int maximumNumberOfLetters = 15;
	
	public static String blackListStatic[] = {
		"nxae","nxui", "nxyu", "nxey", "nxay"
		};
	
	public static String blackList[] = {
		"nxae","nxui", "nxyu", "nxey", "nxay"
	};
	
	public static HashMap<String, IRenderer> allRenderer = new HashMap<String, IRenderer>();
	
	public static void addARenderer (IRenderer aNewRenderer)
	{
		allRenderer.put(aNewRenderer.getClass().getName(), aNewRenderer);
	}
	
	public static void removeARenderer (IRenderer aNewRenderer)
	{
		allRenderer.remove(aNewRenderer.getClass().getName());
	}
	
	public String generatePart(int seed)
	{
		return "0";
	}
	
	public static void generateAllSyllables()
	{
		boolean endOfRank1 = false;
		boolean endOfRank2 = false;
		boolean endOfRank3 = false;
		boolean endOfRank4 = false;
		
		IRenderer myRenderer = new RendererFile("Syllables.txt");
//		IRenderer myRenderer = new RendererSysOut();
		
		GenerateSyllables.SyllablesCursor currentCursor = new GenerateSyllables.SyllablesCursor();
		
		for (int i=0; i < 10000000 ; i++)
		{
			if (!endOfRank1)
			{
				endOfRank1 = !GenerateSyllables.givesSimpleVoyelles(i, currentCursor).exist;
				if (endOfRank1)
				{
					currentCursor.currentPosition = 0;
				}
				myRenderer.renderOneString(i+" :"+currentCursor.syllables+"  ");
			}
			else
			{
				if (!endOfRank2)
				{
					endOfRank2 = !GenerateSyllables.givesVC(currentCursor.currentPosition+1, currentCursor).exist;
					if (endOfRank2)
					{
						currentCursor.currentPosition = 0;
					}
					myRenderer.renderOneString(i+" :"+currentCursor.syllables+"  ");
				}
				else
				{
					if (!endOfRank3)
					{
						endOfRank3 = !GenerateSyllables.givesCV(currentCursor.currentPosition+1, currentCursor).exist;
						if (endOfRank3)
						{
							currentCursor.currentPosition = 0;
						}
						myRenderer.renderOneString(i+" :"+currentCursor.syllables+"  ");
					}
					else
					{
						if (!endOfRank4)
						{
							endOfRank4=!GenerateSyllables.givesCVC(currentCursor.currentPosition+1, currentCursor).exist;
							if (endOfRank4)
							{
								break;
							}
							myRenderer.renderOneString(i+" :"+currentCursor.syllables+"  ");
						}
					}
				}
			}
		}
		myRenderer.closeFile();
	}
	
	public static void generateNames()
	{	
		GenerateSyllables.SyllablesCursor currentCursor = new GenerateSyllables.SyllablesCursor();
		GenerateSyllables.SyllablesCursor currentCursor2 = new GenerateSyllables.SyllablesCursor();
//		GenerateSyllables.SyllablesCursor currentCursor3 = new GenerateSyllables.SyllablesCursor();
//		GenerateSyllables.SyllablesCursor currentCursor4 = new GenerateSyllables.SyllablesCursor();
		
		
		iPart = 1;
		iNbOfGeneratedNames = 0;
		
		// Two "syllables"
		
		// v and cv with cv and cvc
		if (!endAsked)
		{
			generateXVCX(currentCursor, currentCursor2);
		}
		if (!endAsked)
		{
			// vc and cvc with vc and v
			generateXCVX(currentCursor, currentCursor2);
		}
		// Three "syllables"
		
		// v and cv with cv and cvc with cv and v and vc
		
		// vc and cvc with (vc with vc) and (v and cv and cvc)
		
		// Four
		
		// v and cv with cv and cvc with cv and v and vc
		
		// vc and cvc with vc and v with vc and cv and cvc
		
		for (IRenderer oneRenderer : allRenderer.values())
		{
			oneRenderer.closeFile();
		}
		
		Informer.getInstance().getANewInfo("Generation finished");
	}

	/**
	 * v and cv with cv and cvc
	 * @param currentCursor
	 * @param currentCursor2
	 * @param myRenderer
	 */
	private static void generateXVCX(GenerateSyllables.SyllablesCursor currentCursor, GenerateSyllables.SyllablesCursor currentCursor2)
	{
		String part1;
		String part2;
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		ArrayList<IRenderer> allActiveRenderer = new ArrayList<IRenderer>();
		for (IRenderer oneRenderer : allRenderer.values())
		{
			allActiveRenderer.add(oneRenderer);
		}
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		part1 = GenerateSyllables.givesSimpleVoyellesDebut(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesCV(currentCursor2.currentPosition, currentCursor2).syllables;
		
		System.out.println("From "+minimumNumberOfLetters+" to "+maximumNumberOfLetters);
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesCV(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesSimpleVoyellesDebut(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesCV(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
		
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		part1 = GenerateSyllables.givesSimpleVoyellesDebut(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesCVCEnd(currentCursor2.currentPosition, currentCursor2).syllables;
		
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesCVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesSimpleVoyellesDebut(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesCVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
		
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		part1 = GenerateSyllables.givesCV(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesCV(currentCursor2.currentPosition, currentCursor2).syllables;
		
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesCV(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesCV(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesCV(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
		
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		part1 = GenerateSyllables.givesCV(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesCVCEnd(currentCursor2.currentPosition, currentCursor2).syllables;
		
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesCVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesCV(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesCVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}

		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
	}
	
	/**
	 * vc and cvc with vc and v
	 * @param currentCursor
	 * @param currentCursor2
	 * @param myRenderer
	 */
	private static void generateXCVX(GenerateSyllables.SyllablesCursor currentCursor, GenerateSyllables.SyllablesCursor currentCursor2)
	{
		String part1;
		String part2;
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		ArrayList<IRenderer> allActiveRenderer = new ArrayList<IRenderer>();
		for (IRenderer oneRenderer : allRenderer.values())
		{
			allActiveRenderer.add(oneRenderer);
		}
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		part1 = GenerateSyllables.givesVCDebut(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesVCEnd(currentCursor2.currentPosition, currentCursor2).syllables;
		
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesVCDebut(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		part1 = GenerateSyllables.givesVCDebut(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesSimpleVoyelles(currentCursor2.currentPosition, currentCursor2).syllables;
		
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesSimpleVoyelles(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesVCDebut(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesSimpleVoyelles(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		part1 = GenerateSyllables.givesCVC(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesVCEnd(currentCursor2.currentPosition, currentCursor2).syllables;
		
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesCVC(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesVCEnd(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
		
		Informer.getInstance().getANewInfo("Part "+iPart+" processing");
		
		currentCursor.currentPosition = 0;
		currentCursor2.currentPosition = 0;
		
		part1 = GenerateSyllables.givesCVC(currentCursor.currentPosition, currentCursor).syllables;
		part2 = GenerateSyllables.givesSimpleVoyelles(currentCursor2.currentPosition, currentCursor2).syllables;
		
//		System.out.println("Values "+currentCursor.exist+" - "+currentCursor2.exist);
		while (currentCursor.exist && !endAsked)
		{
			while (currentCursor2.exist && !endAsked)
			{
				String resultString = part1+part2;
				if ((resultString.length() >= minimumNumberOfLetters) && (resultString.length() <= maximumNumberOfLetters) && !isBlackListed(resultString))
				{
					for (IRenderer oneRenderer : allActiveRenderer)
					{
						oneRenderer.renderOneString(resultString);
					}
					iNbOfGeneratedNames++;
				}
				part2 = GenerateSyllables.givesSimpleVoyelles(currentCursor2.currentPosition+1, currentCursor2).syllables;
			}
			part1 = GenerateSyllables.givesCVC(currentCursor.currentPosition+1, currentCursor).syllables;
			currentCursor2.currentPosition = 0;
			part2 = GenerateSyllables.givesSimpleVoyelles(currentCursor2.currentPosition+1, currentCursor2).syllables;
			Informer.getInstance().getANewProgress(iNbOfGeneratedNames+" words generated");
		}
		
		for (IRenderer oneRenderer : allActiveRenderer)
		{
			oneRenderer.next();
		}
		Informer.getInstance().getANewInfo("Part "+iPart+" finished");
		iPart++;
	}
	
	public static boolean isBlackListed(String stringToCheck)
	{
		for (int iBlackList = 0; iBlackList < blackList.length ; iBlackList++)
		{
			if (stringToCheck.contains(blackList[iBlackList]))
			{
				return true;
			}
		}
		
		return false;
	}
	public static void stop()
	{
		endAsked = true;
	}
	
	public static void resume()
	{
		endAsked = false;
		// And obviously do something to start back.
	}

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		generateNames();
		System.out.println("Generation finished");
		Informer.getInstance().isFinished(true);
		System.out.println("Informer informed");
	}

	public static void setMinNumberOfLetters(String minNumberOfLetters)
	{
		try
		{
			minimumNumberOfLetters = new Integer(minNumberOfLetters);
			System.out.println("Will have a minimum of "+minimumNumberOfLetters+" letters");
		}
		catch (NumberFormatException e)
		{
			// Ignore it, but log it (not too normal in some cases)
			logger.warn("String cannot be turn into a number ({})",minNumberOfLetters);
		}
	}

	public static void setMaxNumberOfLetters(String maxNumberOfLetters)
	{
		try
		{
			maximumNumberOfLetters = new Integer(maxNumberOfLetters);
		}
		catch (NumberFormatException e)
		{
			// Ignore it, but log it (not too normal in some cases)
			logger.warn("String cannot be turn into a number ({})",maxNumberOfLetters);
		}
		System.out.println("Will have a maximum of "+maximumNumberOfLetters+" letters");
	}
}
