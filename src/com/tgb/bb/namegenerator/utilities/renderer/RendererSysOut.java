/*
 * Created on 9 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities.renderer;

import com.tgb.bb.namegenerator.utilities.IRenderer;

public class RendererSysOut implements IRenderer
{

	public String getNameForUI()
	{
		return "Console (stdOut) renderer";
	}
	
	@Override
	public void renderOneString(String oneString)
	{
		// TODO Auto-generated method stub
		System.out.println(oneString);
	}

	@Override
	public void next()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void next(String name)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void closeFile()
	{
		// TODO Auto-generated method stub
		
	}

}
