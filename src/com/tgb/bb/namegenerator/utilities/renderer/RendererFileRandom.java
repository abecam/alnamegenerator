/*
 * Created on 9 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities.renderer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import com.tgb.bb.namegenerator.utilities.IRenderer;

public class RendererFileRandom implements IRenderer
{
	public static final int LENGTHOFLINE = 120;
	public static final int MAXNUMBEROFLINES = 10000;
	
	File currentFile;
	String fileName;
	FileWriter ourFileOutStream;
	int iCharInLine;
	int iLines; // Number of lines currently written
	int iFiles; // Number of files written
	String folderName ;

	int iNext = 0; // Wait until we reached the next random number to export a name, then generate a new random number and start again.
	int iCurrent = 0;
	
	public String getNameForUI()
	{
		return "Plain text renderer in files with names from random positions";
	}
	
	public RendererFileRandom()
	{
		fileName="Names";
		folderName = "NamesRandom";
		
		checkAndCreateFolder();
		
		currentFile = new File(folderName+"/"+fileName+".txt");
		
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iCharInLine = 0;
		iFiles=1;
		iLines=0;
		
		iNext = (int) (1000*Math.random());
	}
	
	
	
	public RendererFileRandom(String fileName)
	{
		this.fileName=fileName;
		folderName = "NamesRandom";
		
		checkAndCreateFolder();
		
		currentFile = new File(folderName+"/"+fileName+".txt");
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iCharInLine = 0;
		iFiles=1;
	}
	
	public void closeFile()
	{
		try
		{
			ourFileOutStream.close();
		}
		catch (IOException e)
		{
			System.out.println("Problem while closing the file "+currentFile);
			e.printStackTrace();
		};
	}
	
	public String checkAndCreateFolder()
	{
		Date today = GregorianCalendar.getInstance().getTime();
		SimpleDateFormat myFormat = new SimpleDateFormat("_dd-MM-yyyy_HH-mm");
		File rootFolder = new File("./generatedNames/");
		if (!rootFolder.exists())
		{
			rootFolder.mkdir();
		}
		folderName = "./generatedNames/"+folderName+myFormat.format(today);
		File currentFolder = new File(folderName);
		currentFolder.mkdir();
		
		return folderName;
	}
	

	    
	@Override
	public void renderOneString(String oneString)
	{
		// TODO Auto-generated method stub
		if (iCurrent >= iNext)
		{
			iCurrent = 0;
			iNext = (int) (1000*Math.random());

			try
			{
				ourFileOutStream.write(oneString);
				iCharInLine+=(oneString+ ",").length();
				if (iCharInLine > LENGTHOFLINE)
				{
					iCharInLine = 0;
					ourFileOutStream.write("\n");
				}
				else
				{
					ourFileOutStream.write(",");
				}
				iLines++;
			} 
			catch (IOException e)
			{
				System.out.println("Unable to write "+oneString+" to the file "+currentFile);
				e.printStackTrace();
			}
		}
		iCurrent++;
		if (iLines > MAXNUMBEROFLINES)
		{
			this.internalNext();
			iLines = 0;
		}
	}

	private void internalNext()
	{
		// TODO Auto-generated method stub
		closeFile();

		currentFile = new File(folderName+"/"+fileName+"-"+iFiles+".txt");

		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}

		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iCharInLine = 0;
		iFiles++;
	}
	
	@Override
	public void next()
	{
		
	}

	@Override
	public void next(String name)
	{
		// TODO Auto-generated method stub
		
	}
	
}
