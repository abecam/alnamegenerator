/*
 * Created on 9 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities.renderer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tgb.bb.namegenerator.utilities.IRenderer;

public class RendererTemplate implements IRenderer
{
	private static String renderFileName = "Names";
	private static String extensionFileName = ".txt";
	private static final String renderFolderName = "TemplateNames";

	private static Logger logger = LoggerFactory.getLogger(RendererTemplate.class);
			
	public static final int LENGTHOFLINE = 120;
	File currentFile;
	String fileName;
	FileWriter ourFileOutStream;
	int iCharInLine;
	int iFiles; // Number of files written
	String folderName ;
	File template;
	String templatePath = "./templates/Template.txt";
	
	// [BEGINNINGOFPATTERN]StringToPushBeforeName[NAME]StringToPushUntilEndOfLine[ENDOFNAME]StringToPushAtEndOfLine][ENDOFLINE]
	
	//static final String keyForBeginningOfPage = "[BEGINNINGOFPAGE]";
	static final String keyForBeginning = "[BEGINNINGOFPATTERN]";
	static final String keyForBeginningNames = "[BEGINOFNAME]";
	static final String keyForNames = "[NAME]";
	static final String keyForNbInLine = "[ENDOFNAME]";
	static final String keyForNbOfLines = "[ENDOFLINE]";
	
	// Used in replaceAll which need Regex expression so we need to escape the [ and ]
	static final String keyToPlaceNbOfLines = "\\[NBOFLINE\\]";
	static final String keyToPlaceNbOfFile = "\\[NBOFFILE\\]";
	
	String  stringToPushBeforeLine = "";
	String  stringToPushBeforeName = "";
	String  stringToPushUntilEndOfLine = "";
	String  stringToPushAtEndOfLine = "";
	
	String textBeforeKey ="";
	String textAfterKey = "";
	
	int nbInLine = 1;
	int nbOfLines = 100;

	public String getNameForUI()
	{
		return "Renderer using templates (in files)";
	}
	
	public RendererTemplate()
	{
		readValuesFromTemplate();
		
		fileName=renderFileName;
		folderName = renderFolderName;
		
		checkAndCreateFolder();
		
		currentFile = new File(folderName+"/"+fileName+extensionFileName);
		
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iCharInLine = 0;
		iFiles=1;	
		
		renderHeader();
	}
	
	private boolean readValuesFromTemplate()
	{
		template = new File(templatePath);
		
		if (!template.exists())
		{
			logger.error("No template found at "+template.getAbsolutePath()+"!");
			
			return false;
		}
		
		BufferedReader ourFileInStream;
		
		try
		{
			ourFileInStream = new BufferedReader(new FileReader(template));
		} 
		catch (IOException e)
		{
			logger.error("Unable to read from the file "+currentFile, e);
			e.printStackTrace();
			return false;
		}
		
		try
		{
			String readLine= "";
			
			int currentStep = 0;
			// 0->Before key
			// 1->In key
			// 2->After key
			
			readLine = ourFileInStream.readLine();
			
			StringBuffer currentString = new StringBuffer();
			
			while (readLine != null)
			{
				treatALine(readLine, currentStep, currentString);
				
				readLine = ourFileInStream.readLine();
			}
			
			textAfterKey = currentString.toString();
		} 
		catch (IOException e)
		{
			logger.error("Unable to write to the file "+currentFile,e);
		}
		try
		{
			ourFileInStream.close();
		} 
		catch (IOException e)
		{
			logger.error("Problem while closing the stream",e);
			e.printStackTrace();
		}
		return false;
	}
	
	boolean isBeginning = true;
	boolean gotNbInLine = false;
	boolean gotNbOfLines = false;
	boolean gotFileName = false;
	boolean gotFileExtension = false;
	
	private void treatALine(String currentLine, int currentStep, StringBuffer currentString)
	{
		if (isBeginning)
		{
			if (currentLine.contains("nbInLine"))
			{
				String nbInLineTxt = currentLine.substring(currentLine.indexOf("=") + 1).trim();
				nbInLine = new Integer(nbInLineTxt);
				
				gotNbInLine = true;
			}
			if (currentLine.contains("nbOfLines"))
			{
				String nbInLineTxt = currentLine.substring(currentLine.indexOf("=") + 1).trim();
				nbOfLines = new Integer(nbInLineTxt);
				
				gotNbOfLines = true;
			}
			if (currentLine.contains("nameOfFile"))
			{
				renderFileName = currentLine.substring(currentLine.indexOf("=") + 1).trim();
				
				gotFileName = true;
			}
			if (currentLine.contains("extensionOfFile"))
			{
				extensionFileName = currentLine.substring(currentLine.indexOf("=") + 1).trim();
				
				gotFileExtension = true;
			}
			
			isBeginning = !(gotNbInLine & gotNbOfLines & gotFileName & gotFileExtension);
		}
		else
		{
			StringBuffer tmpLine = new StringBuffer(currentLine);
			//		static final String keyForBeginning = "[BEGINNINGOFPATTERN]";
			//		static final String keyForNames = "[NAME]";
			//		static final String keyForNbInLine = "[NBINLINE]";
			//		static final String keyForNbOfLines = "[NBOFLINES]";
			//		static final String keyForEnd = "[ENDOFPATTERN]";

			if (currentStep == 0)
			{
				String keyToUse = keyForBeginning;
				StringBuffer tmpBuffer = new StringBuffer();
				int tmpStep = currentStep;

				currentStep = checkLineForKey(tmpLine, currentStep, currentString, keyToUse, tmpBuffer);

				logger.info("TmpLine is "+tmpLine);
				if (currentStep > tmpStep)
				{
					textBeforeKey = tmpBuffer.toString();
				}		
			}
			if (currentStep == 1)
			{
				String keyToUse = keyForBeginningNames;
				StringBuffer tmpBuffer = new StringBuffer();
				int tmpStep = currentStep;

				currentStep = checkLineForKey(tmpLine, currentStep, currentString, keyToUse, tmpBuffer);

				logger.info("TmpLine is "+tmpLine);
				if (currentStep > tmpStep)
				{
					stringToPushBeforeLine = tmpBuffer.toString();
				}
			}
			if (currentStep == 2)
			{
				String keyToUse = keyForNames;
				StringBuffer tmpBuffer = new StringBuffer();
				int tmpStep = currentStep;

				currentStep = checkLineForKey(tmpLine, currentStep, currentString, keyToUse, tmpBuffer);

				logger.info("TmpLine is "+tmpLine);
				if (currentStep > tmpStep)
				{
					stringToPushBeforeName = tmpBuffer.toString();
				}
			}
			if (currentStep == 3)
			{
				// [BEGINNINGOFPATTERN]StringToPushBeforeName[NAME]StringToPushUntilEndOfLine[NBINLINE*nbInLine]StringToPushAtEndOfLine]StringToPushAtEndOfFile[NBOFFLINE**nbOfLines][ENDOFPATTERN]

				String keyToUse = keyForNbInLine;
				StringBuffer tmpBuffer = new StringBuffer();
				int tmpStep = currentStep;

				currentStep = checkLineForKey(tmpLine, currentStep, currentString, keyToUse, tmpBuffer);

				logger.info("TmpLine is "+tmpLine);
				if (currentStep > tmpStep)
				{
					stringToPushUntilEndOfLine = tmpBuffer.toString();
				}
			}
			if (currentStep == 4)
			{
				// [BEGINNINGOFPATTERN]StringToPushBeforeName[NAME]StringToPushUntilEndOfLine[NBINLINE*nbInLine]StringToPushAtEndOfLine]StringToPushAtEndOfFile[NBOFFLINE**nbOfLines][ENDOFPATTERN]

				String keyToUse = keyForNbOfLines;
				StringBuffer tmpBuffer = new StringBuffer();
				int tmpStep = currentStep;

				currentStep = checkLineForKey(tmpLine, currentStep, currentString, keyToUse, tmpBuffer);

				logger.info("TmpLine is "+tmpLine);
				if (currentStep > tmpStep)
				{
					stringToPushAtEndOfLine = tmpBuffer.toString();
				}
			}
			if (currentStep == 5)
			{
				// All the rest
				currentString.append(tmpLine+"\n");
				
				logger.info("currentString is "+currentString);
			}
		}
	}

	/**
	 * @param currentLine
	 * @param currentStep
	 * @param currentString
	 * @param keyToUse
	 * @param tmpBuffer
	 * @return
	 */
	private int checkLineForKey(StringBuffer currentLine, int currentStep, StringBuffer currentString, String keyToUse, StringBuffer tmpBuffer)
	{
		if (currentLine.toString().contains(keyToUse))
		{
			currentStep++;
			// Add everything which is before in the line
			String allBeforeKey = currentLine.substring(0,currentLine.indexOf(keyToUse));
			currentString.append(allBeforeKey);
			
			tmpBuffer.append(currentString.toString());
			
			currentString.delete(0, currentString.length());
			
			String allAfterKey = currentLine.substring(currentLine.indexOf(keyToUse)+keyToUse.length());
			
			logger.info("tmpBuffer is "+tmpBuffer);
			logger.info("allAfterKey is "+allAfterKey);
			// Now keep the remaining part for the next check-up
			currentLine.delete(0, currentLine.length());
			
			currentLine.append(allAfterKey);
		}
		else
		{
			currentString.append(currentLine+"\n");
		}
		return currentStep;
	}
	
	public void closeFile()
	{
		try
		{
			ourFileOutStream.close();
		}
		catch (IOException e)
		{
			System.out.println("Problem while closing the file "+currentFile);
			e.printStackTrace();
		};
	}
	
	public String checkAndCreateFolder()
	{
		Date today = GregorianCalendar.getInstance().getTime();
		SimpleDateFormat myFormat = new SimpleDateFormat("_dd-MM-yyyy_HH-mm");
		File rootFolder = new File("./generatedNames/");
		if (!rootFolder.exists())
		{
			rootFolder.mkdir();
		}
		folderName = "./generatedNames/"+folderName+myFormat.format(today);
		File currentFolder = new File(folderName);
		currentFolder.mkdir();
		
		return folderName;
	}
	
	int iLines = 0;
	int iWordInLine = 0;
	    
	@Override
	public void renderOneString(String oneString)
	{
//		static final String keyForBeginning = "[BEGINNINGOFPATTERN]";
//		static final String keyForNames = "[NAME]";
//		static final String keyForNbInLine = "[ENDOFNAME]";
//		static final String keyForNbOfLines = "[ENDOFLINE]";
//		
//		String  stringToPushBeforeName = "";
//		String  stringToPushUntilEndOfLine = "";
//		String  stringToPushAtEndOfLine = "";
//		
//		String textBeforeKey ="";
//		String textAfterKey = "";
		try
		{
			ourFileOutStream.write(stringToPushBeforeName.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+""));
			ourFileOutStream.write(oneString);
			
			iWordInLine++;
			
			if (iWordInLine > nbInLine)
			{
				iWordInLine = 0;

				ourFileOutStream.write(stringToPushAtEndOfLine.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+"")+"\n");
				
				iLines++;
				
				if (iLines <= nbOfLines)
				{
					ourFileOutStream.write(stringToPushBeforeLine.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+""));
				}
			}
			else
			{
				ourFileOutStream.write(stringToPushUntilEndOfLine.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+""));
			}
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write "+oneString+" to the file "+currentFile);
			e.printStackTrace();
		}
		if (iLines > nbOfLines)
		{
			this.internalNext();
			iLines = 0;
		}
	}

	private void internalNext()
	{
		// First print the footer
		renderFooter();
		
		closeFile();
		
		currentFile = new File(folderName+"/"+fileName+"-"+iFiles+extensionFileName);
		
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iWordInLine = 0;
		iFiles++;
		
		renderHeader();
	}
	
	private void renderHeader()
	{
		try
		{
			ourFileOutStream.write(textBeforeKey.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+""));
			ourFileOutStream.write(stringToPushBeforeLine.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+""));
		}
		catch (IOException e)
		{
			System.out.println("Unable to write " + textBeforeKey + " to the file " + currentFile);
			e.printStackTrace();
		}
	}

	private void renderFooter()
	{
		try
		{
			ourFileOutStream.write(textAfterKey.replaceAll(keyToPlaceNbOfFile, iFiles+"").replaceAll(keyToPlaceNbOfLines, iLines+""));
		}
		catch (IOException e)
		{
			System.out.println("Unable to write " + textAfterKey + " to the file " + currentFile);
			e.printStackTrace();
		}
	}

	@Override
	public void next()
	{
		// TODO Auto-generated method stub
		closeFile();
		
		currentFile = new File(folderName+"/"+fileName+"-"+iFiles+extensionFileName);
		
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iCharInLine = 0;
		iFiles++;
	}

	@Override
	public void next(String name)
	{
		// TODO Auto-generated method stub
		
	}
	
}
