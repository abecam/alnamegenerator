/*
 * Created on 9 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities.renderer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import com.tgb.bb.namegenerator.utilities.IRenderer;

public class RendererHTML implements IRenderer
{
	public static final int LENGTHOFLINE = 15;
	public static final int MAXNUMBEROFLINES = 300;
	File currentFile;
	String fileName;
	FileWriter ourFileOutStream;
	int iWordInLine;
	int iLines; // Number of lines currently written
	int iFiles; // Number of files written
	String folderName ;
	private boolean isInTR = false;
	private boolean alt = true;

	public String getNameForUI()
	{
		return "HTML renderer with tabs";
	}
	
	public RendererHTML()
	{
		fileName="Names";
		folderName = "NamesHTML";
		
		checkAndCreateFolder();
		
		currentFile = new File(folderName+"/"+fileName+"-0.html");
		
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iWordInLine = 0;
		iFiles=1;
		iLines=0;
		
		renderHeader();
	}
	
	public RendererHTML(String fileName)
	{
		this.fileName=fileName;
		folderName = "NamesHTML";
		
		checkAndCreateFolder();
		
		currentFile = new File(folderName+"/"+fileName+".txt");
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iWordInLine = 0;
		iFiles=1;
		iLines=0;
		
		renderHeader();
	}
	
	public void closeFile()
	{
		try
		{
			ourFileOutStream.close();
		}
		catch (IOException e)
		{
			System.out.println("Problem while closing the file "+currentFile);
			e.printStackTrace();
		};
	}
	
	public String checkAndCreateFolder()
	{
		Date today = GregorianCalendar.getInstance().getTime();
		SimpleDateFormat myFormat = new SimpleDateFormat("_dd-MM-yyyy_HH-mm");
		File rootFolder = new File("./generatedNames/");
		if (!rootFolder.exists())
		{
			rootFolder.mkdir();
		}
		folderName = "./generatedNames/"+folderName+myFormat.format(today);
		File currentFolder = new File(folderName);
		currentFolder.mkdir();
		
		return folderName;
	}
	

	    
	@Override
	public void renderOneString(String oneString)
	{
		// TODO Auto-generated method stub
		try
		{
			ourFileOutStream.write("<TD>");
			ourFileOutStream.write(oneString);
			ourFileOutStream.write("</TD>");
			
			iWordInLine++;
			
			if (iWordInLine > LENGTHOFLINE)
			{
				iWordInLine = 0;
				ourFileOutStream.write("\n");
				isInTR = true;
				if (alt)
				{
					ourFileOutStream.write("</TR><TR style=\"background-color: #999999;\">");
					alt = false;
				}
				else
				{
					ourFileOutStream.write("</TR><TR style=\"background-color: #FFFFFF;\">");
					alt = true;
				}
				iLines++;
			}
			else
			{
				ourFileOutStream.write(" ");
				isInTR = false;
			}
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write "+oneString+" to the file "+currentFile);
			e.printStackTrace();
		}
		if (iLines > MAXNUMBEROFLINES)
		{
			this.internalNext();
			iLines = 0;
		}
	}

	private void internalNext()
	{
		// First print the footer
		renderFooter();
		
		closeFile();
		
		currentFile = new File(folderName+"/"+fileName+"-"+iFiles+".html");
		
		if (currentFile.exists())
		{
			currentFile.delete();
		}
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			System.out.println("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
		iWordInLine = 0;
		iFiles++;
		
		renderHeader();
	}
	
	@Override
	public void next()
	{
		// Nothing here
	}

	@Override
	public void next(String name)
	{
		// TODO Auto-generated method stub
		
	}
	
	private void renderHeader()
	{
		try
		{
			ourFileOutStream.write("<html><head><title>Al'Name generator, page "+iFiles+"</title></head><body bgcolor=white marginheight=\"0\" marginwidth=\"0\">");
			ourFileOutStream.write("<div style=\"background:#DE5533;\">");
			ourFileOutStream.write("<table><tr><td>");
			ourFileOutStream.write("<h1 style=\"margin-left: 40px;display:inline;\">Al'Name generator, page "+iFiles+"</h1>");
			ourFileOutStream.write("</td>\n\n</td></tr></table>\n\n</div>");
			
			ourFileOutStream.write("<center>");
			ourFileOutStream.write("<table style=\"width: 1280px;\" halign=\"left\"><tr><td>");
			ourFileOutStream.write("<table style=\"width: 1280px;\"><tr><td>");
			
			if (iFiles > 1)
			{
				ourFileOutStream.write("<a href=\"names-"+(iFiles-2)+".html\">Previous page</a>");
			}
			ourFileOutStream.write("</td><td>");
			ourFileOutStream.write("<a href=\"names-"+iFiles+".html\">Next page</a>");
			ourFileOutStream.write("</td><td>");
			if (iFiles > 11)
			{
				ourFileOutStream.write("<a href=\"names-"+(iFiles-11)+".html\">-10 pages</a>");
			}
			ourFileOutStream.write("</td><td>");
			if (iFiles > 101)
			{
				ourFileOutStream.write("<a href=\"names-"+(iFiles-101)+".html\">-100 pages</a>");
			}
			ourFileOutStream.write("</td><td>");
			ourFileOutStream.write("<a href=\"names-"+(iFiles+9)+".html\">+10 pages</a>");
			ourFileOutStream.write("</td><td>");
			ourFileOutStream.write("<a href=\"names-"+(iFiles+99)+".html\">+100 pages</a>");
			ourFileOutStream.write("</td><td>");
			if (iFiles > 1)
			{
				ourFileOutStream.write("<a href=\"names-0.html\">First page</a>");
			}
			ourFileOutStream.write("</td></tr></table>");
			ourFileOutStream.write("<br/><br/><br/>");
			ourFileOutStream.write("<table cellpadding=\"2\" cellspacing=\"0\"><tr>");
			
			alt = true;
		}
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
	}
	
	private void renderFooter()
	{
		try
		{
			if (isInTR)
			{
				// First close the TR
				ourFileOutStream.write("</TR>");
			}
			ourFileOutStream.write("</table>");
			ourFileOutStream.write("<br/><br/><br/>");
			ourFileOutStream.write("<table style=\"width: 1280px;\"><tr><td>");
			
			if (iFiles > 1)
			{
				ourFileOutStream.write("<a href=\"names-"+(iFiles-2)+".html\">Previous page</a>");
			}
			ourFileOutStream.write("</td><td>");
			ourFileOutStream.write("<a href=\"names-"+iFiles+".html\">Next page</a>");
			ourFileOutStream.write("</td><td>");
			if (iFiles > 11)
			{
				ourFileOutStream.write("<a href=\"names-"+(iFiles-11)+".html\">-10 pages</a>");
			}
			ourFileOutStream.write("</td><td>");
			if (iFiles > 101)
			{
				ourFileOutStream.write("<a href=\"names-"+(iFiles-101)+".html\">-100 pages</a>");
			}
			ourFileOutStream.write("</td><td>");
			ourFileOutStream.write("<a href=\"names-"+(iFiles+9)+".html\">+10 pages</a>");
			ourFileOutStream.write("</td><td>");
			ourFileOutStream.write("<a href=\"names-"+(iFiles+99)+".html\">+100 pages</a>");
			ourFileOutStream.write("</td><td>");
			if (iFiles > 1)
			{
				ourFileOutStream.write("<a href=\"names-0.html\">First page</a>");
			}
			ourFileOutStream.write("</td></tr></table>");
			
			ourFileOutStream.write("</td></tr></table>");
			ourFileOutStream.write("</center>");

			ourFileOutStream.write("</body></html>");
		}
		catch (IOException e)
		{
			System.out.println("Unable to write to the file "+currentFile);
			e.printStackTrace();
		}
	}
}
