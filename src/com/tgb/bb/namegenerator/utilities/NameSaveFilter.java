/*
 * Created on 25 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

import java.io.File;
import java.io.FilenameFilter;

import javax.swing.filechooser.FileFilter;

public class NameSaveFilter extends FileFilter implements FilenameFilter
{

	@Override
	public boolean accept(File f) {
	    if (f.isDirectory()) {
		return true;
	    }

	    String extension = NameSaveFilter.getExtension(f);
	    if (extension != null) {
	    	if (extension.equals("sav"))
	    	{
	    		return true;
	    	} 
	    	else 
	    	{
	    		return false;
	    	}
	    }

	    return false;
	}

	@Override
	public String getDescription()
	{
		// TODO Auto-generated method stub
		return "NameGenerator save files only";
	}

	/*
     * Get the extension of a file.
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }

	@Override
	public boolean accept(File folderName, String fileName)
	{
		String extension = null;

		int i = fileName.lastIndexOf('.');

		if (i > 0 &&  i < fileName.length() - 1) 
		{
			extension = fileName.substring(i+1).toLowerCase();
		}


		if (extension != null) 
		{
			if (extension.equals("sav"))
			{
				return true;
			} 
			else 
			{
				return false;
			}
		}
		return false;
	}

}
