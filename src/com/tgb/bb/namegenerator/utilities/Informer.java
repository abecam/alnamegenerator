/*
 * Created on 19 fvr. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

/**
 * Singleton which can inform registered classes. Currently one only (the gui)
 * @author Alain
 *
 */
public class Informer
{
	private static Informer instance = null;
	private IInfoReceiver myReceiver;
	private boolean isFinished = false;
	
	synchronized static public Informer getInstance()
	{
		if (instance == null)
		{
			instance = new Informer();
		}
		return instance;
	}
	
	public void registerMyReceiver(IInfoReceiver oneReceiver)
	{
		myReceiver = oneReceiver;
	}
	
	public void getANewInfo(String oneNewInfo)
	{
		if (myReceiver != null)
		{
			myReceiver.receiveInfoString(oneNewInfo);
		}
	}
	
	public void getANewProgress(String oneNewInfo)
	{
		if (myReceiver != null)
		{
			myReceiver.receiveProgressString(oneNewInfo);
		}
	}

	public void isFinished(boolean isItFinished)
	{
		// TODO Auto-generated method stub
		isFinished  = isItFinished;
		if (myReceiver != null)
		{
			myReceiver.isFinished(isItFinished);
		}
	}
}
