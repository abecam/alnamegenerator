/*
 * Created on 25 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tgb.bb.namegenerator.GenerateNames;
import com.tgb.bb.namegenerator.GenerateSyllables;

public class LoadSaveConfig
{
	private static Logger myLogger = LoggerFactory.getLogger(GenerateSyllables.class);
	
	private File fileUsed; // For information, or it is not useful to keep it here
	
	public String checkAndCreateFile()
	{
		String fileName;
		
		Date today = GregorianCalendar.getInstance().getTime();
		SimpleDateFormat myFormat = new SimpleDateFormat("_dd-MM-yyyy_HH-mm");
		File rootFolder = new File("saveParameters/");
		if (!rootFolder.exists())
		{
			rootFolder.mkdir();
		}

		fileName = "saveParameters/config"+myFormat.format(today)+".sav";
		myLogger.info("Filename generated {}",fileName);
		
		return fileName;
	}
	
	public void saveCurrentConfigToFile(File forName)
	{
		File currentFile;
		
		if (forName != null)
		{
			currentFile = forName;
		}
		else
		{
			myLogger.info("No file to save the config to, will use the default one");
			String fileName = checkAndCreateFile();
			currentFile = new File(fileName);
		}
		
		fileUsed = currentFile;
		
		FileWriter ourFileOutStream;
		
		try
		{
			currentFile.createNewFile();
		} 
		catch (IOException e1)
		{
			myLogger.error("Unable to create to the file "+currentFile);
			e1.printStackTrace();
		}
		
		try
		{
			ourFileOutStream = new FileWriter(currentFile);
		} 
		catch (IOException e)
		{
			myLogger.error("Unable to write to the file "+currentFile);
			e.printStackTrace();
			return;
		}
		
		try
		{
			ourFileOutStream.write("Voyelles= ");
			ourFileOutStream.write(giveStringOfVoyelles());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("Consonnes= ");
			ourFileOutStream.write(giveStringOfConsonnes());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("DoubleAndTripleVoyelles= ");
			ourFileOutStream.write(giveStringOfDoubleAndTripleVoyelles());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("DoubleConsonnesMiddle= ");
			ourFileOutStream.write(giveStringOfDoubleConsonnesMiddle());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("TripleConsonnesMiddle= ");
			ourFileOutStream.write(giveStringOfTripleConsonnesMiddle());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("DoubleAndTripleVoyellesDebut= ");
			ourFileOutStream.write(giveStringOfDoubleAndTripleVoyellesDebut());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("DoubleConsonnesDebut= ");
			ourFileOutStream.write(giveStringOfDoubleConsonnesDebut());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("TripleConsonnesDebut= ");
			ourFileOutStream.write(giveStringOfTripleConsonnesDebut());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("DoubleConsonnesFin= ");
			ourFileOutStream.write(giveStringOfDoubleConsonnesFin());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("TripleConsonnesFin= ");
			ourFileOutStream.write(giveStringOfTripleConsonnesFin());
			ourFileOutStream.write("\n");
			ourFileOutStream.write("BlackList= ");
			ourFileOutStream.write(giveStringOfBlackList());
			ourFileOutStream.write("\n");
			
			myLogger.info("Voyelles= ");
			myLogger.info(giveStringOfVoyelles());
			myLogger.info("\n");
			myLogger.info("Consonnes= ");
			myLogger.info(giveStringOfConsonnes());
			myLogger.info("\n");
			myLogger.info("DoubleAndTripleVoyelles= ");
			myLogger.info(giveStringOfDoubleAndTripleVoyelles());
			myLogger.info("\n");
			myLogger.info("DoubleConsonnesMiddle= ");
			myLogger.info(giveStringOfDoubleConsonnesMiddle());
			myLogger.info("\n");
			myLogger.info("TripleConsonnesMiddle= ");
			myLogger.info(giveStringOfTripleConsonnesMiddle());
			myLogger.info("\n");
			myLogger.info("DoubleAndTripleVoyellesDebut= ");
			myLogger.info(giveStringOfDoubleAndTripleVoyellesDebut());
			myLogger.info("\n");
			myLogger.info("DoubleConsonnesDebut= ");
			myLogger.info(giveStringOfDoubleConsonnesDebut());
			myLogger.info("\n");
			myLogger.info("TripleConsonnesDebut= ");
			myLogger.info(giveStringOfTripleConsonnesDebut());
			myLogger.info("\n");
			myLogger.info("DoubleConsonnesFin= ");
			myLogger.info(giveStringOfDoubleConsonnesFin());
			myLogger.info("\n");
			myLogger.info("TripleConsonnesFin= ");
			myLogger.info(giveStringOfTripleConsonnesFin());
			myLogger.info("\n");
			myLogger.info("BlackList= ");
			myLogger.info(giveStringOfBlackList());
			myLogger.info("\n");
		} 
		catch (IOException e)
		{
			myLogger.error("Unable to write to the file "+currentFile,e);
			e.printStackTrace();
		}
		try
		{
			ourFileOutStream.close();
		} 
		catch (IOException e)
		{
			myLogger.error("Problem while closing the stream",e);
			e.printStackTrace();
		}
	}
	
	public void loadConfigFromFile(File forName)
	{
		File currentFile;
		
		if (forName != null)
		{
			currentFile = forName;
		}
		else
		{
			myLogger.error("No file to load the config from");
			return;
		}
		fileUsed = currentFile;
		
		BufferedReader ourFileInStream;
		
		try
		{
			ourFileInStream = new BufferedReader(new FileReader(currentFile));
		} 
		catch (IOException e)
		{
			myLogger.error("Unable to read from the file "+currentFile, e);
			return;
		}
		
		try
		{
			String readLine= "";
			String configString = "";
			
			readLine = ourFileInStream.readLine();
			
			while (readLine != null)
			{
				if (readLine.startsWith("Voyelles= "))
				{
					configString = readLine.substring("Voyelles= ".length());
					
					GenerateSyllables.voyelles = setListFromEntry(configString);
				}

				if (readLine.startsWith("Consonnes= "))
				{
					configString = readLine.substring("Consonnes= ".length());
					
					GenerateSyllables.consonnes = setListFromEntry(configString);
				}

				if (readLine.startsWith("DoubleAndTripleVoyelles= "))
				{
					configString = readLine.substring("DoubleAndTripleVoyelles= ".length());
					
					GenerateSyllables.okDoubleVoyelles = setListFromEntry(configString);
				}

				if (readLine.startsWith("DoubleConsonnesMiddle= "))
				{
					configString = readLine.substring("DoubleConsonnesMiddle= ".length());
					
					GenerateSyllables.okDoubleConsonnes = setListFromEntry(configString);
				}

				if (readLine.startsWith("TripleConsonnesMiddle= "))
				{
					configString = readLine.substring("TripleConsonnesMiddle= ".length());
					
					GenerateSyllables.okTripleConsonnes = setListFromEntry(configString);
				}

				if (readLine.startsWith("DoubleAndTripleVoyellesDebut= "))
				{
					configString = readLine.substring("DoubleAndTripleVoyellesDebut= ".length());
					
					GenerateSyllables.okDoubleVoyellesDebut = setListFromEntry(configString);
				}

				if (readLine.startsWith("DoubleConsonnesDebut= "))
				{
					configString = readLine.substring("DoubleConsonnesDebut= ".length());
					
					GenerateSyllables.okDoubleConsonnesDebut = setListFromEntry(configString);
				}

				if (readLine.startsWith("TripleConsonnesDebut= "))
				{
					configString = readLine.substring("TripleConsonnesDebut= ".length());
					
					GenerateSyllables.okTripleConsonnesDebut = setListFromEntry(configString);
				}

				if (readLine.startsWith("DoubleConsonnesFin= "))
				{
					configString = readLine.substring("DoubleConsonnesFin= ".length());
					
					GenerateSyllables.okDoubleConsonnesEnd = setListFromEntry(configString);
				}

				if (readLine.startsWith("TripleConsonnesFin= "))
				{
					configString = readLine.substring("TripleConsonnesFin= ".length());
					
					GenerateSyllables.okTripleConsonnesEnd = setListFromEntry(configString);
				}
				
				if (readLine.startsWith("BlackList= "))
				{
					configString = readLine.substring("BlackList= ".length());
					
					GenerateNames.blackList = setListFromEntry(configString);
				}
				
				myLogger.info("Line read : "+readLine);
				myLogger.info("Line found : "+configString);
				
				readLine = ourFileInStream.readLine();
			}
		} 
		catch (IOException e)
		{
			myLogger.error("Unable to read from the file "+currentFile,e);
		}
		try
		{
			ourFileInStream.close();
		} 
		catch (IOException e)
		{
			myLogger.error("Problem while closing the stream",e);
		}
	}
	
	protected String[] setListFromEntry(String cvsString)
	{
		myLogger.info("Splitting from "+cvsString);
		String stringOfElements = cvsString.replace(" ", "");
		myLogger.info("Splitting from "+stringOfElements);
		
		String[] splittedString = stringOfElements.split(",");
		
		return splittedString;
	}
	
	/**
	 * @return
	 */
	private String giveStringOfVoyelles()
	{
		String stringOfVoyelles="";
		for (int iVoyelles = 0; iVoyelles < GenerateSyllables.voyelles.length-1 ; iVoyelles++)
		{
			stringOfVoyelles+=GenerateSyllables.voyelles[iVoyelles];
			stringOfVoyelles+=", ";
		}
		if (GenerateSyllables.voyelles.length != 0)
		{
			stringOfVoyelles+=GenerateSyllables.voyelles[GenerateSyllables.voyelles.length-1];
		}
		
		return stringOfVoyelles;
	}

	private String giveStringOfConsonnes()
	{
		String stringOfConsonnes="";
		for (int iConsonnes = 0; iConsonnes < GenerateSyllables.consonnes.length-1 ; iConsonnes++)
		{
			stringOfConsonnes+=GenerateSyllables.consonnes[iConsonnes];
			stringOfConsonnes+=", ";
		}
		if (GenerateSyllables.consonnes.length != 0)
		{
			stringOfConsonnes+=GenerateSyllables.consonnes[GenerateSyllables.consonnes.length-1];
		}

		return stringOfConsonnes;
	}
	
	private String giveStringOfDoubleAndTripleVoyelles()
	{
		String stringOfDoubleAndTripleVoyelles="";
		for (int iDoubleAndTripleVoyelles = 0; iDoubleAndTripleVoyelles < GenerateSyllables.okDoubleVoyelles.length-1 ; iDoubleAndTripleVoyelles++)
		{
			stringOfDoubleAndTripleVoyelles+=GenerateSyllables.okDoubleVoyelles[iDoubleAndTripleVoyelles];
			stringOfDoubleAndTripleVoyelles+=", ";
		}
		if (GenerateSyllables.okDoubleVoyelles.length != 0)
		{
			stringOfDoubleAndTripleVoyelles+=GenerateSyllables.okDoubleVoyelles[GenerateSyllables.okDoubleVoyelles.length-1];
		}
		

		return stringOfDoubleAndTripleVoyelles;
	}
	
	private String giveStringOfDoubleConsonnesMiddle()
	{
		String stringOfDoubleConsonnesMiddle="";
		for (int iDoubleConsonnesMiddle = 0; iDoubleConsonnesMiddle < GenerateSyllables.okDoubleConsonnes.length-1 ; iDoubleConsonnesMiddle++)
		{
			stringOfDoubleConsonnesMiddle+=GenerateSyllables.okDoubleConsonnes[iDoubleConsonnesMiddle];
			stringOfDoubleConsonnesMiddle+=", ";
		}
		if (GenerateSyllables.okDoubleConsonnes.length != 0)
		{
			stringOfDoubleConsonnesMiddle+=GenerateSyllables.okDoubleConsonnes[GenerateSyllables.okDoubleConsonnes.length-1];
		}
		return stringOfDoubleConsonnesMiddle;
	}
	
	private String giveStringOfTripleConsonnesMiddle()
	{
		String stringOfTripleConsonnesMiddle="";
		for (int iTripleConsonnesMiddle = 0; iTripleConsonnesMiddle < GenerateSyllables.okTripleConsonnes.length-1 ; iTripleConsonnesMiddle++)
		{
			stringOfTripleConsonnesMiddle+=GenerateSyllables.okTripleConsonnes[iTripleConsonnesMiddle];
			stringOfTripleConsonnesMiddle+=", ";
		}
		if (GenerateSyllables.okTripleConsonnes.length != 0)
		{
			stringOfTripleConsonnesMiddle+=GenerateSyllables.okTripleConsonnes[GenerateSyllables.okTripleConsonnes.length-1];
		}
		
		return stringOfTripleConsonnesMiddle;
	}
	
	private String giveStringOfDoubleAndTripleVoyellesDebut()
	{
		String stringOfDoubleAndTripleVoyellesDebut="";
		for (int iDoubleAndTripleVoyellesDebut = 0; iDoubleAndTripleVoyellesDebut < GenerateSyllables.okDoubleVoyellesDebut.length-1 ; iDoubleAndTripleVoyellesDebut++)
		{
			stringOfDoubleAndTripleVoyellesDebut+=GenerateSyllables.okDoubleVoyellesDebut[iDoubleAndTripleVoyellesDebut];
			stringOfDoubleAndTripleVoyellesDebut+=", ";
		}
		if (GenerateSyllables.okDoubleVoyellesDebut.length != 0)
		{
			stringOfDoubleAndTripleVoyellesDebut+=GenerateSyllables.okDoubleVoyellesDebut[GenerateSyllables.okDoubleVoyellesDebut.length-1];
		}
		
		return stringOfDoubleAndTripleVoyellesDebut;
	}
	
	private String giveStringOfDoubleConsonnesDebut()
	{
		String stringOfDoubleConsonnesDebut="";
		for (int iDoubleConsonnesDebut = 0; iDoubleConsonnesDebut < GenerateSyllables.okDoubleConsonnesDebut.length-1 ; iDoubleConsonnesDebut++)
		{
			stringOfDoubleConsonnesDebut+=GenerateSyllables.okDoubleConsonnesDebut[iDoubleConsonnesDebut];
			stringOfDoubleConsonnesDebut+=", ";
		}
		if (GenerateSyllables.okDoubleConsonnesDebut.length != 0)
		{
			stringOfDoubleConsonnesDebut+=GenerateSyllables.okDoubleConsonnesDebut[GenerateSyllables.okDoubleConsonnesDebut.length-1];
		}
		
		return stringOfDoubleConsonnesDebut;
	}
	
	private String giveStringOfTripleConsonnesDebut()
	{
		String stringOfTripleConsonnesDebut="";
		for (int iTripleConsonnesDebut = 0; iTripleConsonnesDebut < GenerateSyllables.okTripleConsonnesDebut.length-1 ; iTripleConsonnesDebut++)
		{
			stringOfTripleConsonnesDebut+=GenerateSyllables.okTripleConsonnesDebut[iTripleConsonnesDebut];
			stringOfTripleConsonnesDebut+=", ";
		}
		if (GenerateSyllables.okTripleConsonnesDebut.length != 0)
		{
			stringOfTripleConsonnesDebut+=GenerateSyllables.okTripleConsonnesDebut[GenerateSyllables.okTripleConsonnesDebut.length-1];
		}
		
		return stringOfTripleConsonnesDebut;
	}
	
	private String giveStringOfDoubleConsonnesFin()
	{
		String stringOfDoubleConsonnesFin="";
		for (int iDoubleConsonnesFin = 0; iDoubleConsonnesFin < GenerateSyllables.okDoubleConsonnesEnd.length-1 ; iDoubleConsonnesFin++)
		{
			stringOfDoubleConsonnesFin+=GenerateSyllables.okDoubleConsonnesEnd[iDoubleConsonnesFin];
			stringOfDoubleConsonnesFin+=", ";
		}
		if (GenerateSyllables.okDoubleConsonnesEnd.length != 0)
		{
			stringOfDoubleConsonnesFin+=GenerateSyllables.okDoubleConsonnesEnd[GenerateSyllables.okDoubleConsonnesEnd.length-1];
		}
		
		return stringOfDoubleConsonnesFin;
	}
	
	private String giveStringOfTripleConsonnesFin()
	{
		String stringOfTripleConsonnesFin="";
		for (int iTripleConsonnesFin = 0; iTripleConsonnesFin < GenerateSyllables.okTripleConsonnesEnd.length-1 ; iTripleConsonnesFin++)
		{
			stringOfTripleConsonnesFin+=GenerateSyllables.okTripleConsonnesEnd[iTripleConsonnesFin];
			stringOfTripleConsonnesFin+=", ";
		}
		if (GenerateSyllables.okTripleConsonnesEnd.length != 0)
		{
			stringOfTripleConsonnesFin+=GenerateSyllables.okTripleConsonnesEnd[GenerateSyllables.okTripleConsonnesEnd.length-1];
		}
		
		return stringOfTripleConsonnesFin;
	}

	private String giveStringOfBlackList()
	{
		String stringOfBlackList="";
		for (int iBlackList = 0; iBlackList < GenerateNames.blackList.length-1 ; iBlackList++)
		{
			stringOfBlackList+=GenerateNames.blackList[iBlackList];
			stringOfBlackList+=", ";
		}
		if (GenerateNames.blackList.length != 0)
		{
			stringOfBlackList+=GenerateNames.blackList[GenerateNames.blackList.length-1];
		}
		
		return stringOfBlackList;
	}
	
	/**
	 * @return the fileUsed
	 */
	public File getFileUsed()
	{
		return fileUsed;
	}
}
