/*
 * Created on 20 fvr. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

import java.awt.event.KeyEvent;
import javax.swing.JTextField;

public class IntegerJTextField extends JTextField
{
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	final static String badchars = "`~!@#$%^&*()_+=\\|\"':;?/>.<, ";

	public IntegerJTextField(String string)
	{
		// TODO Auto-generated constructor stub
		super(string);
	}

	public void processKeyEvent(KeyEvent ev)
	{

		char c = ev.getKeyChar();

		if ((Character.isLetter(c) && !ev.isAltDown()) || badchars.indexOf(c) > -1)
		{
			ev.consume();
			return;
		}
		if (c == '-' && getDocument().getLength() > 0)
			ev.consume();
		else
			super.processKeyEvent(ev);

	}

}
