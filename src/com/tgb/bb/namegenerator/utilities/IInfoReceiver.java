/*
 * Created on 19 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

public interface IInfoReceiver
{
	public void receiveInfoString(String aNewInfo);
	
	public void receiveProgressString(String progress);
	
	public void isFinished(boolean isItFinished);
}
