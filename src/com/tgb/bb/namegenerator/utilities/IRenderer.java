/*
 * Created on 9 fvr. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

public interface IRenderer
{
	/**
	 * Return a short descriptive name
	 */
	public String getNameForUI();
	
	/**
	 * Somehow render the given string.
	 * @param oneString
	 */
	public void renderOneString(String oneString);
	
	/**
	 *  If we are creating stupidly several "files", we can use this method to go to the next
	 */
	public void next();
	
	/**
	 * If we are creating stupidly several "files", we can use this method to go to the next, with a name
	 */
	public void next(String name);

	/**
	 * If dealing with a file, close it
	 */
	public void closeFile();
}
