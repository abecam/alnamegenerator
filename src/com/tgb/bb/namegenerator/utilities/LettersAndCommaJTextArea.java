/*
 * Created on 20 fvr. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.utilities;

import java.awt.event.KeyEvent;
import javax.swing.JTextArea;

public class LettersAndCommaJTextArea extends JTextArea
{
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	final static String badchars = "`~!@#$%^&*()_+=\\|\"':;?/>.<";

	public void processKeyEvent(KeyEvent ev)
	{

		char c = ev.getKeyChar();

		if ((Character.isDigit(c) && !ev.isAltDown()) || badchars.indexOf(c) > -1)
		{
			ev.consume();
			return;
		}
		
		super.processKeyEvent(ev);
	}

}
