/*
 * Created on 5 Feb. 11
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Try to generates a set of syllables, create Java-compatible lists
 * @author Alain
 *
 */
public class GenerateSyllables
{
	private static Logger myLogger = LoggerFactory.getLogger(GenerateSyllables.class);
	
	public enum Types{SIMPLE_VOYELLE,SIMPLE_VOYELLE_DEBUT,VC,CV,CVC,
						VC_DEBUT,VC_END,CVC_END};
	public static class SyllablesCursor
	{
		String syllables = "";
		int currentPosition = 0;
		boolean exist = true;
	}
	
	public final static String consonnesStatic[]= 
		{"b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","z"};
	
	public final static String voyellesStatic[]= 
		{"a","e","i","o","u","y"};
	
	public final static String okDoubleConsonnesStatic[] =
	{
		"mb","mc","md","mf","mg","mh","mj","mk","ml","mm","mn","mp","ms","mv","mw","mx","ch",
		"hc","ph","hp","wh","hw","hm","nb","nc","nd","ng","nh","nj","nk","nl","nm","nn","np",
		"nq","nr","ns","nt","nv","nw","nx","pl"
	};
	
	public final static String okTripleConsonnesStatic[] =
	{
		"nth","mth","nch","mch","nph","mph","chr","khr","thr"
	};
	
	public final static String okDoubleConsonnesDebutStatic[] =
	{
		"ch","ph","wh"
	};
	
	public final static String okTripleConsonnesDebutStatic[] =
	{
		"chr","khr","thr"
	};
	
	public final static String okDoubleConsonnesEndStatic[] =
	{
		"mb","mc","md","mm","ch",
		"ph"
		
//		"mb","mc","md","mf","mg","mh","mj","mk","ml","mm","mn","mp","ms","mv","mw","mx","ch",
//		"hc","ph","hp","wh","hw","hm","nb","nc","nd","ng","nh","nj","nk","nl","nm","nn","np",
//		"nq","nr","ns","nt","nv","nw","nx"
	};
	
	public final static String okTripleConsonnesEndStatic[] =
	{
		"nth","mth","nch","mch","nph","mph"
	};
	
	// Double AND triple voyelles
	public final static String okDoubleVoyellesStatic[] =
	{
		"aa","ae","ea","ya","ey","ay","ye","yu","ui","iu","oi","io","ue","eu","ee","ieu","oei","ain","in"
	};

	public final static String okDoubleVoyellesDebutStatic[] =
	{
		"ya","ay","ye","yu","ui","oi","eu","oei"
	};

	/*
	 * Dynamics ones
	 */
	public static String consonnes[]= 
	{"b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","z"};
	
	public static String voyelles[]= 
	{"a","e","i","o","u","y"};

	public static String okDoubleConsonnes[] =
	{
		"mb","mc","md","mf","mg","mh","mj","mk","ml","mm","mn","mp","ms","mv","mw","mx","ch",
		"hc","ph","hp","wh","hw","hm","nb","nc","nd","ng","nh","nj","nk","nl","nm","nn","np",
		"nq","nr","ns","nt","nv","nw","nx","pl"
	};

	public static String okTripleConsonnes[] =
	{
		"nth","mth","nch","mch","nph","mph","chr","khr","thr"
	};

	public static String okDoubleConsonnesDebut[] =
	{
		"ch","ph","wh"
	};

	public static String okTripleConsonnesDebut[] =
	{
		"chr","khr","thr"
	};

	public static String okDoubleConsonnesEnd[] =
	{
		"mb","mc","md","mm","ch",
		"ph"

		//	"mb","mc","md","mf","mg","mh","mj","mk","ml","mm","mn","mp","ms","mv","mw","mx","ch",
		//	"hc","ph","hp","wh","hw","hm","nb","nc","nd","ng","nh","nj","nk","nl","nm","nn","np",
		//	"nq","nr","ns","nt","nv","nw","nx"
	};

	public static String okTripleConsonnesEnd[] =
	{
		"nth","mth","nch","mch","nph","mph"
	};

	// Double AND triple voyelles
	public static String okDoubleVoyelles[] =
	{
		"aa","ae","ea","ya","ey","ay","ye","yu","ui","iu","oi","io","ue","eu","ee","ieu","oei"
	};

	public static String okDoubleVoyellesDebut[] =
	{
		"ya","ay","ye","yu","ui","oi","eu","oei"
	};
	/**
	 * Using simple assumptions, try to generate a set of syllabus
	 */ 
	public static void givesSyllabus()
	{
		int iInLine = 0;
		
//		for (char rank1 = 'a'; rank1 <= 'z'; ++rank1)
//		{
//			for (char rank2 = 'a'; rank2 <= 'z'; ++rank2)
//			{
//				for (char rank3 = 'a'; rank3 <= 'z'; ++rank3)
//				{
//					for (char rank4 = 'a'; rank4 <= 'z'; ++rank4)
//					{
//						for (char rank5 = 'a'; rank5 <= 'z'; ++rank5)
//						{
//							char sequence[] = {rank1,rank2,rank3,rank4,rank5}; 
//							String resultString = new String(sequence);
//							
//							System.out.print("\""+resultString+"\" , ");
//							iInLine+=resultString.length();
//							
//							if (iInLine > 150)
//							{
//								System.out.println();
//								iInLine = 0;
//							}
//						}
//					}
//				}
//			}
//		}
		
		iInLine = generatesSimpleVoyelles(iInLine);
		System.out.println("\n2 elements vc\n------------------------------------------------------");
		
		iInLine = generateVC(iInLine);
		System.out.println("\n2 elements cv\n------------------------------------------------------");

		iInLine = generateCV(iInLine);
		
			
		System.out.println("\n3 elements\n------------------------------------------------------");
		// 3 elements, only cvc
		iInLine = generateCVC(iInLine);
	}
	/**
	 * @param iInLine
	 * @return
	 */
	public static int generateCVC(int iInLine)
	{
		for (int rank1= 0; rank1 < consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length; ++rank1)
		{
			System.out.println("\nRank "+rank1+"\n------------------------------------------------------");
			
			String resultString;

			// Groups of voyelles

			if (rank1 < consonnes.length)
			{
				resultString=consonnes[rank1];
			}
			else if (rank1 < consonnes.length+ okDoubleConsonnesDebut.length )
			{
				resultString=okDoubleConsonnesDebut[rank1-consonnes.length];
			}
			else // if (rank1 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
			{
				resultString=okTripleConsonnesDebut[rank1-consonnes.length-okDoubleConsonnesDebut.length];
			}

			// 2 elements
			for (int rank2= 0; rank2 < voyelles.length+okDoubleVoyelles.length; ++rank2)
			{
				// 2 elements
				// cv

				String resultString2;

				if (rank2 < voyelles.length)
				{
					resultString2=voyelles[rank2];
				}
				else //if (rank2 < voyelles.length + okDoubleVoyelles.length)
				{
					resultString2=okDoubleVoyelles[rank2-voyelles.length];
				}

				for (int rank3= 0; rank3 < consonnes.length+okDoubleConsonnes.length+okTripleConsonnes.length; ++rank3)
				{
					String resultString3;

					// Groups of voyelles

					if (rank3 < consonnes.length)
					{
						resultString3=consonnes[rank3];
					}
					else if (rank3 < consonnes.length+ okDoubleConsonnes.length )
					{
						resultString3=okDoubleConsonnes[rank3-consonnes.length];
					}
					else // if (rank3 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
					{
						resultString3=okTripleConsonnes[rank3-consonnes.length-okDoubleConsonnes.length];
					}

//					if ((resultString+resultString2+resultString3).equals("rjmh"))
//					{
//						System.out.println("\n 1: "+resultString+"  2: "+resultString2+"  3: "+resultString3);
//					}

					System.out.print("\""+resultString+resultString2+resultString3+"\" , ");
					iInLine+=("\""+resultString+resultString2+resultString3+"\" , ").length();

					if (iInLine > 150)
					{
						System.out.println();
						iInLine = 0;
					}
				}
			}			
		}
		return iInLine;
	}
	/**
	 * @param iInLine
	 * @return
	 */
	public static int generateCV(int iInLine)
	{
		for (int rank1= 0; rank1 < consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length; ++rank1)
		{
			String resultString;

			if (rank1 < consonnes.length)
			{
				resultString=consonnes[rank1];
			}
			else if (rank1 < consonnes.length+okDoubleConsonnesDebut.length )
			{
				resultString=okDoubleConsonnesDebut[rank1-consonnes.length];
			}
			else // if (rank1 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
			{
				resultString=okTripleConsonnesDebut[rank1-consonnes.length-okDoubleConsonnesDebut.length];
			}

			// 2 elements
			for (int rank2= 0; rank2 < voyelles.length+okDoubleVoyelles.length; ++rank2)
			{
				// 2 elements
				// cv

				String resultString2;

				if (rank2 < voyelles.length)
				{
					resultString2=voyelles[rank2];
				}
				else //if (rank2 < voyelles.length + okDoubleVoyelles.length)
				{
					resultString2=okDoubleVoyelles[rank2-voyelles.length];
				}

				System.out.print("\""+resultString+resultString2+"\" , ");
				iInLine+=("\""+resultString+resultString2+"\" , ").length();

				if (iInLine > 150)
				{
					System.out.println();
					iInLine = 0;
				}
			}
		}
		return iInLine;
	}
	/**
	 * @param iInLine
	 * @return
	 */
	public static int generateVC(int iInLine)
	{
		for (int rank1= 0; rank1 < voyelles.length+okDoubleVoyelles.length; ++rank1)
		{
			String resultString;
			
			
				if (rank1 < voyelles.length)
				{
					resultString=voyelles[rank1];
				}
				else //if (rank1 < voyelles.length + okDoubleVoyelles.length)
				{
					resultString=okDoubleVoyelles[rank1-voyelles.length];
				}

				// 2 elements
				for (int rank2= 0; rank2 < consonnes.length+okDoubleConsonnes.length+okTripleConsonnes.length; ++rank2)
				{
					// 2 elements
					// vc

					String resultString2;

					if (rank2 < consonnes.length)
					{
						resultString2=consonnes[rank2];
					}
					else if (rank2 < consonnes.length+ okDoubleConsonnes.length )
					{
						resultString2=okDoubleConsonnes[rank2-consonnes.length];
					}
					else // if (rank2 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
					{
						resultString2=okTripleConsonnes[rank2-consonnes.length-okDoubleConsonnes.length];
					}

					System.out.print("\""+resultString+resultString2+"\" , ");
					iInLine+=("\""+resultString+resultString2+"\" , ").length();

					if (iInLine > 150)
					{
						System.out.println();
						iInLine = 0;
					}
				}
		}
		return iInLine;
	}
	/**
	 * @param iInLine
	 * @return
	 */
	private static int generatesSimpleVoyelles(int iInLine)
	{
		// Only voyelles
		for (int rank1= 0; rank1 < voyelles.length+okDoubleVoyelles.length; ++rank1)
		{
			String resultString;
			
			if (rank1 < voyelles.length)
			{
				resultString=voyelles[rank1];
			}
			else
			{
				resultString=okDoubleVoyelles[rank1-voyelles.length];
			}
			
			System.out.print("\""+resultString+"\" , ");
			iInLine+=("\""+resultString+"\" , ").length();
			
			if (iInLine > 150)
			{
				System.out.println();
				iInLine = 0;
			}
		}
		return iInLine;
	}

	/**
	 * @param iInLine
	 * @return
	 */
	public static int generatesSimpleVoyellesDebut(int iInLine)
	{
		// Only voyelles
		for (int rank1= 0; rank1 < voyelles.length+okDoubleVoyellesDebut.length; ++rank1)
		{
			String resultString;
			
			if (rank1 < voyelles.length)
			{
				resultString=voyelles[rank1];
			}
			else
			{
				resultString=okDoubleVoyellesDebut[rank1-voyelles.length];
			}
			
			System.out.print("\""+resultString+"\" , ");
			iInLine+=("\""+resultString+"\" , ").length();
			
			if (iInLine > 150)
			{
				System.out.println();
				iInLine = 0;
			}
		}
		return iInLine;
	}

	///
	// Same versions but with parameters: i,j,k -> kind of seed...
	// and as cursor-> start, next.
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesSimpleVoyelles(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 2
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 1
		if (posGlobal >= (voyelles.length+okDoubleVoyelles.length))
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank

		int rank1 = posGlobal;
				
		
		myLogger.debug("V - Rank "+rank1+"  --  ");

		String resultString;
		
		// Only voyelles
		{
			
			if (rank1 < voyelles.length)
			{
				resultString=voyelles[rank1];
			}
			else
			{
				resultString=okDoubleVoyelles[rank1-voyelles.length];
			}
			
			myLogger.debug("\""+resultString+"\" , ");
		}
		
		oneNewCursor.syllables = resultString;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesSimpleVoyellesDebut(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 2
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 1
		if (posGlobal >= (voyelles.length+okDoubleVoyellesDebut.length))
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank

		int rank1 = posGlobal;
				
		
		myLogger.debug("V - Rank "+rank1+"  --  ");

		String resultString;
		
		// Only voyelles
		{
			
			if (rank1 < voyelles.length)
			{
				resultString=voyelles[rank1];
			}
			else
			{
				resultString=okDoubleVoyellesDebut[rank1-voyelles.length];
			}
			
			myLogger.debug("\""+resultString+"\" , ");
		}
		
		oneNewCursor.syllables = resultString;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesVC(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 2
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 1
		if (posGlobal >= (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
		* (voyelles.length+okDoubleVoyelles.length))
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank
		
		int rank2 = posGlobal / (voyelles.length+okDoubleVoyelles.length);
		
		int rank1 = posGlobal - (rank2*(voyelles.length+okDoubleVoyelles.length));
				
		
		myLogger.debug("VC - Rank "+rank1+"  --  "+rank2);

		String resultString;
		String resultString2;
		
			
		if (rank1 < voyelles.length)
		{
			resultString=voyelles[rank1];
		}
		else //if (rank1 < voyelles.length + okDoubleVoyelles.length)
		{
			resultString=okDoubleVoyelles[rank1-voyelles.length];
		}

		// 2 elements

		{
			// 2 elements
			// vc


			if (rank2 < consonnes.length)
			{
				resultString2=consonnes[rank2];
			}
			else if (rank2 < consonnes.length+ okDoubleConsonnes.length )
			{
				resultString2=okDoubleConsonnes[rank2-consonnes.length];
			}
			else // if (rank2 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
			{
				resultString2=okTripleConsonnes[rank2-consonnes.length-okDoubleConsonnes.length];
			}

			myLogger.debug("\""+resultString+resultString2+"\" , ");

		}

		oneNewCursor.syllables = resultString+resultString2;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesVCDebut(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 2
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 1
		if (posGlobal >= (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
		* (voyelles.length+okDoubleVoyellesDebut.length))
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank
		
		int rank2 = posGlobal / (voyelles.length+okDoubleVoyellesDebut.length);
		
		int rank1 = posGlobal - (rank2*(voyelles.length+okDoubleVoyellesDebut.length));
				
		
		myLogger.debug("VC - Rank "+rank1+"  --  "+rank2);

		String resultString;
		String resultString2;
		
			
		if (rank1 < voyelles.length)
		{
			resultString=voyelles[rank1];
		}
		else //if (rank1 < voyelles.length + okDoubleVoyelles.length)
		{
			resultString=okDoubleVoyellesDebut[rank1-voyelles.length];
		}

		// 2 elements

		{
			// 2 elements
			// vc


			if (rank2 < consonnes.length)
			{
				resultString2=consonnes[rank2];
			}
			else if (rank2 < consonnes.length+ okDoubleConsonnesDebut.length )
			{
				resultString2=okDoubleConsonnesDebut[rank2-consonnes.length];
			}
			else // if (rank2 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
			{
				resultString2=okTripleConsonnesDebut[rank2-consonnes.length-okDoubleConsonnesDebut.length];
			}

			myLogger.debug("\""+resultString+resultString2+"\" , ");

		}

		oneNewCursor.syllables = resultString+resultString2;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesVCEnd(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 2
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 1
		if (posGlobal >= (consonnes.length+okDoubleConsonnesEnd.length+okTripleConsonnesEnd.length)
		* (voyelles.length+okDoubleVoyelles.length))
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank
		
		int rank2 = posGlobal / (voyelles.length+okDoubleVoyelles.length);
		
		int rank1 = posGlobal - (rank2*(voyelles.length+okDoubleVoyelles.length));
				
		
		myLogger.debug("VC - Rank "+rank1+"  --  "+rank2);

		String resultString;
		String resultString2;
		
			
		if (rank1 < voyelles.length)
		{
			resultString=voyelles[rank1];
		}
		else //if (rank1 < voyelles.length + okDoubleVoyelles.length)
		{
			resultString=okDoubleVoyelles[rank1-voyelles.length];
		}

		// 2 elements

		{
			// 2 elements
			// vc


			if (rank2 < consonnes.length)
			{
				resultString2=consonnes[rank2];
			}
			else if (rank2 < consonnes.length+ okDoubleConsonnesEnd.length )
			{
				resultString2=okDoubleConsonnesEnd[rank2-consonnes.length];
			}
			else // if (rank2 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
			{
				resultString2=okTripleConsonnesEnd[rank2-consonnes.length-okDoubleConsonnesEnd.length];
			}

			myLogger.debug("\""+resultString+resultString2+"\" , ");

		}

		oneNewCursor.syllables = resultString+resultString2;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesCV(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 1
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 2
		if (posGlobal >= (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
		* (voyelles.length+okDoubleVoyelles.length))
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank
		
		int rank2 = posGlobal / (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length);
		
		int rank1 = posGlobal - (rank2*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length));
				
		
		myLogger.debug("CV - Rank "+rank1+"  --  "+rank2);

		String resultString;
		String resultString2;
		
		{
			if (rank1 < consonnes.length)
			{
				resultString=consonnes[rank1];
			}
			else if (rank1 < consonnes.length+okDoubleConsonnesDebut.length )
			{
				resultString=okDoubleConsonnesDebut[rank1-consonnes.length];
			}
			else // if (rank1 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
			{
				resultString=okTripleConsonnesDebut[rank1-consonnes.length-okDoubleConsonnesDebut.length];
			}

			// 2 elements
			{
				// 2 elements
				// cv

				if (rank2 < voyelles.length)
				{
					resultString2=voyelles[rank2];
				}
				else //if (rank2 < voyelles.length + okDoubleVoyelles.length)
				{
					resultString2=okDoubleVoyelles[rank2-voyelles.length];
				}

				myLogger.debug("\""+resultString+resultString2+"\" , ");
			}
		}
		oneNewCursor.syllables = resultString+resultString2;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}

	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesCVC(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 1
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 2
		// * (consonnes.length+okDoubleConsonnes.length+okTripleConsonnes.length)			----- Rank 3
		if (posGlobal >= (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
		* (voyelles.length+okDoubleVoyelles.length) 										
		* (consonnes.length+okDoubleConsonnes.length+okTripleConsonnes.length)			)
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank
		int rank3 = posGlobal / ((consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
								* (voyelles.length+okDoubleVoyelles.length));
		
		int rank2 = (posGlobal -(rank3*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
								* (voyelles.length+okDoubleVoyelles.length))) / (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length);
		
		int rank1 = (posGlobal -(rank3*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
				* (voyelles.length+okDoubleVoyelles.length))) - (rank2*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length));
				
		
		myLogger.debug("CVC -"+posGlobal+ " - Rank "+rank1+"  --  "+rank2+"  --  "+rank3);

		String resultString;
		String resultString2;
		String resultString3;
		// Groups of voyelles

		if (rank1 < consonnes.length)
		{
			resultString=consonnes[rank1];
		}
		else if (rank1 < consonnes.length+ okDoubleConsonnesDebut.length )
		{
			resultString=okDoubleConsonnesDebut[rank1-consonnes.length];
		}
		else // if (rank1 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
		{
			resultString=okTripleConsonnesDebut[rank1-consonnes.length-okDoubleConsonnesDebut.length];
		}

		// 2 elements

		{
			// 2 elements
			// cv

			

			if (rank2 < voyelles.length)
			{
				resultString2=voyelles[rank2];
			}
			else //if (rank2 < voyelles.length + okDoubleVoyelles.length)
			{
				resultString2=okDoubleVoyelles[rank2-voyelles.length];
			}


			{
				

				// Groups of voyelles

				if (rank3 < consonnes.length)
				{
					resultString3=consonnes[rank3];
				}
				else if (rank3 < consonnes.length+ okDoubleConsonnes.length )
				{
					resultString3=okDoubleConsonnes[rank3-consonnes.length];
				}
				else // if (rank3 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
				{
					resultString3=okTripleConsonnes[rank3-consonnes.length-okDoubleConsonnes.length];
				}

//				if ((resultString+resultString2+resultString3).equals("rjmh"))
//				{
//				myLogger.infoln(" 1: "+resultString+"  2: "+resultString2+"  3: "+resultString3);
//				}

				myLogger.debug("\""+resultString+resultString2+resultString3+"\" , ");
			}
		}			

		oneNewCursor.syllables = resultString+resultString2+resultString3;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	/**
	 * @param iInLine
	 * @return
	 */
	public static SyllablesCursor givesCVCEnd(int posGlobal,SyllablesCursor currentCursor)
	{
		if (currentCursor == null)
		{
			currentCursor = new SyllablesCursor();
		}
		SyllablesCursor oneNewCursor = currentCursor;
		// Total possibilities:
		//   (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length) ----- Rank 1
		// * (voyelles.length+okDoubleVoyelles.length) 										----- Rank 2
		// * (consonnes.length+okDoubleConsonnes.length+okTripleConsonnes.length)			----- Rank 3
		if (posGlobal >= (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
		* (voyelles.length+okDoubleVoyelles.length) 										
		* (consonnes.length+okDoubleConsonnesEnd.length+okTripleConsonnesEnd.length)			)
		{
			// Finished the range
			oneNewCursor.syllables = "";
			oneNewCursor.exist = false;
			oneNewCursor.currentPosition = posGlobal;
			
			return oneNewCursor;
		}
		
		// Now find the various rank
		int rank3 = posGlobal / ((consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
								* (voyelles.length+okDoubleVoyelles.length));
		
		int rank2 = (posGlobal -(rank3*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
								* (voyelles.length+okDoubleVoyelles.length))) / (consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length);
		
		int rank1 = (posGlobal -(rank3*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length)
				* (voyelles.length+okDoubleVoyelles.length))) - (rank2*(consonnes.length+okDoubleConsonnesDebut.length+okTripleConsonnesDebut.length));
				
		
		myLogger.debug("CVC -"+posGlobal+ " - Rank "+rank1+"  --  "+rank2+"  --  "+rank3);

		String resultString;
		String resultString2;
		String resultString3;
		// Groups of voyelles

		if (rank1 < consonnes.length)
		{
			resultString=consonnes[rank1];
		}
		else if (rank1 < consonnes.length+ okDoubleConsonnesDebut.length )
		{
			resultString=okDoubleConsonnesDebut[rank1-consonnes.length];
		}
		else // if (rank1 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
		{
			resultString=okTripleConsonnesDebut[rank1-consonnes.length-okDoubleConsonnesDebut.length];
		}

		// 2 elements

		{
			// 2 elements
			// cv

			

			if (rank2 < voyelles.length)
			{
				resultString2=voyelles[rank2];
			}
			else //if (rank2 < voyelles.length + okDoubleVoyelles.length)
			{
				resultString2=okDoubleVoyelles[rank2-voyelles.length];
			}


			{
				

				// Groups of voyelles

				if (rank3 < consonnes.length)
				{
					resultString3=consonnes[rank3];
				}
				else if (rank3 < consonnes.length+ okDoubleConsonnesEnd.length )
				{
					resultString3=okDoubleConsonnesEnd[rank3-consonnes.length];
				}
				else // if (rank3 < voyelles.length + consonnes.length+ okDoubleVoyelles.length +okDoubleConsonnes.length +okTripleConsonnes.length )
				{
					resultString3=okTripleConsonnesEnd[rank3-consonnes.length-okDoubleConsonnesEnd.length];
				}

//				if ((resultString+resultString2+resultString3).equals("rjmh"))
//				{
//				myLogger.infoln(" 1: "+resultString+"  2: "+resultString2+"  3: "+resultString3);
//				}

				myLogger.debug("\""+resultString+resultString2+resultString3+"\" , ");
			}
		}			

		oneNewCursor.syllables = resultString+resultString2+resultString3;
		oneNewCursor.exist = true;
		oneNewCursor.currentPosition = posGlobal;

		return oneNewCursor;
	}
	
	public static SyllablesCursor givesSyllable(int posGlobal,SyllablesCursor currentCursor, Types typeOfSyllable)
	{
		switch (typeOfSyllable)
		{
			case SIMPLE_VOYELLE:
				return givesSimpleVoyelles(posGlobal, currentCursor);

			case SIMPLE_VOYELLE_DEBUT:
				return givesSimpleVoyellesDebut(posGlobal, currentCursor);
			case VC:
				return givesVC(posGlobal, currentCursor);
			case CV:
				return givesCV(posGlobal, currentCursor);
			case CVC:
				return givesCVC(posGlobal, currentCursor);
			case VC_DEBUT:
				return givesVCDebut(posGlobal, currentCursor);
			case VC_END:
				return givesVCEnd(posGlobal, currentCursor);
			case CVC_END:
				return givesCVCEnd(posGlobal, currentCursor);
		}
		return currentCursor;
	}
}

