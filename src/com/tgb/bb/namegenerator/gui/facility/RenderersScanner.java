/*
 * Created on 1 juil. 2020
 *
 */

/*
 * Copyright (c)2020 Alain Becam
 */

package com.tgb.bb.namegenerator.gui.facility;

import java.io.File;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tgb.bb.namegenerator.utilities.IRenderer;

public class RenderersScanner
{
	private static final String IRENDERER_FULLNAME = "com.tgb.bb.namegenerator.utilities.IRenderer";
	private static final String RENDERER_PACKAGE_NAME = "com.tgb.bb.namegenerator.utilities.renderer";

	private static final String RENDERER_PACKAGE_FOLDER = "./classes/com/tgb/bb/namegenerator/utilities/renderer";
	
	private static Logger logger = LoggerFactory.getLogger(RenderersScanner.class);
	
	static String[] getClassInRenderePackage()
	{
		File rendererFolder = new File(RENDERER_PACKAGE_FOLDER);
		
		if (!rendererFolder.exists())
		{
			logger.error("Renderer folder does not exist! ("+rendererFolder.getAbsolutePath()+" )");
			
			return null;
		}
		if (!rendererFolder.isDirectory())
		{
			logger.error("Renderer folder has probably been replaced by a file!");
			
			return null;
		}
		
		String[] allFilesInRendererFolder = rendererFolder.list();
		
		for (String oneString : allFilesInRendererFolder)
		{
			System.out.println("Found "+oneString);
		}
		
		return allFilesInRendererFolder;
	}

	public static ArrayList<IRenderer> getAllCurrentRenderers()
	{
		ArrayList<IRenderer> allRendererFound = new ArrayList<>();
		
		String[] allFileInRendererPackage = getClassInRenderePackage();
		
		if (allFileInRendererPackage != null)
		{
			for (String oneFile : allFileInRendererPackage)
			{
				String oneClassname = oneFile.replace(".class", "");
				
				if (isImplementingIRenderer(oneClassname))
				{
					allRendererFound.add(getRendererClass(oneClassname));
				}
			}
		}
		return allRendererFound;
	}
	
	static boolean isImplementingIRenderer(String className)
	{
		Class<?>[] setOfInterfaces;
        boolean isImplemented = false; // Indicate if the given interface is
        // implemented by the given class
        try
        {
            // Here we just verify the class exist in the module repository (we are working locally...)
            Class<?> exploredClass = Class.forName(RENDERER_PACKAGE_NAME+"."+className);

            // And not destructs it just after...
            try
            {
                Class<?> exploredInterface = Class.forName(IRENDERER_FULLNAME);
                // Exception to manage, if no class for this name !
                if (!exploredInterface.isInterface())
                {
                	logger.error("IRenderer is not an interface, please check your installation!");
                }
                setOfInterfaces = exploredClass.getInterfaces();
                for (int i = 0; i < setOfInterfaces.length; i++)
                {
                    if (setOfInterfaces[i] == exploredInterface)
                    {
                        isImplemented = true;
                    }
                }
                if (!isImplemented)
                {
                	logger.info("IRenderer is not implemented by this class");
                    return false; // To be enhanced.
                }

                return true;
            } catch (Exception e)
            {
            	logger.error("Could not check the interface for this class ", e);
                return false; // To be enhanced!!!
            }
        } catch (Exception e)
        {
        	logger.error("Could not get this class ", e);
            return false; // To be enhanced!!!
        }
	}
	
	static IRenderer getRendererClass(String className)
	{
        // implemented by the given class
        try
        {
            // Here we just verify the class exist in the module repository (we are working locally...)
            Class<IRenderer> exploredClass = (Class<IRenderer>) Class.forName(RENDERER_PACKAGE_NAME+"."+className);
            
            return exploredClass.newInstance();

        } catch (Exception e)
        {
        	logger.error("Could not get this class ", e);
            return null; // To be enhanced!!!
        }
	}
}
