/*
 * Created on 12 Feb. 2011
 *
 */

/*
 * Copyright (c)2011 Alain Becam
 */

package com.tgb.bb.namegenerator.gui;

import java.io.File;
import java.util.ArrayList;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Insets;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tgb.bb.namegenerator.GenerateNames;
import com.tgb.bb.namegenerator.GenerateSyllables;
import com.tgb.bb.namegenerator.gui.facility.RenderersScanner;
import com.tgb.bb.namegenerator.utilities.IInfoReceiver;
import com.tgb.bb.namegenerator.utilities.IRenderer;
import com.tgb.bb.namegenerator.utilities.Informer;
import com.tgb.bb.namegenerator.utilities.IntegerJTextField;
import com.tgb.bb.namegenerator.utilities.LettersAndCommaJTextArea;
import com.tgb.bb.namegenerator.utilities.LoadSaveConfig;
import com.tgb.bb.namegenerator.utilities.NameSaveFilter;



public class MainWindow extends JFrame implements IInfoReceiver
{
	private static Logger logger = LoggerFactory.getLogger(GenerateSyllables.class);
	
	// Bricks to create the names
	JLabel labelListVoyelles;
	JTextArea listOfVoyelles;
	LettersAndCommaJTextArea entryListOfVoyelles;
	JRadioButton radioVoyellesUsed;
	JButton resetVoyelles;
	
	JLabel labelConsonnes;
	JTextArea listOfConsonnes;
	LettersAndCommaJTextArea entryListOfConsonnes;
	JRadioButton radioConsonnesUsed;
	JButton resetConsonnes;
	
	JLabel labelDoubleAndTripleVoyelles;
	JTextArea listOfDoubleAndTripleVoyelles;
	LettersAndCommaJTextArea entryListOfDoubleAndTripleVoyelles;
	JRadioButton radioDoubleAndTripleVoyellesUsed;
	JButton resetDoubleAndTripleVoyelles;
	
	JLabel labelDoubleConsonnesMiddle;
	JTextArea listOfDoubleConsonnesMiddle;
	LettersAndCommaJTextArea entryListOfDoubleConsonnesMiddle;
	JRadioButton radioDoubleConsonnesMiddleUsed;
	JButton resetDoubleConsonnesMiddle;
	
	JLabel labelTripleConsonnesMiddle;
	JTextArea listOfTripleConsonnesMiddle;
	LettersAndCommaJTextArea entryListOfTripleConsonnesMiddle;
	JRadioButton radioTripleConsonnesMiddleUsed;
	JButton resetTripleConsonnesMiddle;
	
	JLabel labelDoubleAndTripleVoyellesDebut;
	JTextArea listOfDoubleAndTripleVoyellesDebut;
	LettersAndCommaJTextArea entryListOfDoubleAndTripleVoyellesDebut;
	JRadioButton radioDoubleAndTripleVoyellesDebutUsed;
	JButton resetDoubleAndTripleVoyellesDebut;
	
	JLabel labelDoubleConsonnesDebut;
	JTextArea listOfDoubleConsonnesDebut;
	LettersAndCommaJTextArea entryListOfDoubleConsonnesDebut;
	JRadioButton radioDoubleConsonnesDebutUsed;
	JButton resetDoubleConsonnesDebut;
	
	JLabel labelTripleConsonnesDebut;
	JTextArea listOfTripleConsonnesDebut;
	LettersAndCommaJTextArea entryListOfTripleConsonnesDebut;
	JRadioButton radioTripleConsonnesDebutUsed;
	JButton resetTripleConsonnesDebut;
	
	JLabel labelDoubleConsonnesFin;
	JTextArea listOfDoubleConsonnesFin;
	LettersAndCommaJTextArea entryListOfDoubleConsonnesFin;
	JRadioButton radioDoubleConsonnesFinUsed;
	JButton resetDoubleConsonnesFin;
	
	JLabel labelTripleConsonnesFin;
	JTextArea listOfTripleConsonnesFin;
	LettersAndCommaJTextArea entryListOfTripleConsonnesFin;
	JRadioButton radioTripleConsonnesFinUsed;
	JButton resetTripleConsonnesFin;
	
	// Wanted combinations
	JLabel labelCombinations;
	JList listOfCombinations;
	JTextArea entryListOfCombinations;
	
	// Black list, sequence of letters we never want to have
	JLabel labelBlackList;
	JList blackList;
	LettersAndCommaJTextArea entryBlackList;
	
	JToggleButton doDeleteButton;
	// Generate button
	JButton generateButton;
	
	JTextArea infoArea;
	JTextArea nbOfCombinationsInfo;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JLabel lblMinimumNumberOf;
	private JLabel lblMaximumNumberOf;
	private IntegerJTextField entryMaximumNumberOf;
	private IntegerJTextField entryMinimumNumberOf;
	private JMenuBar menuBar;
	private JMenuItem mntmSaveSettings;
	private JMenuItem mntmOpenSettings;
	private JMenu mnAbout;
	private JMenu mnFile;
	private JMenuItem mntmOpen;
	private JMenuItem mntmSave;

	public MainWindow() throws HeadlessException
	{
		super();

		Informer.getInstance().registerMyReceiver(this);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0};
		getContentPane().setLayout(gridBagLayout);
		
		GridBagConstraints c = new GridBagConstraints();	
		c.insets = new Insets(0, 0, 5, 5);
		// Create the various elements
		labelListVoyelles = new JLabel ("Voyelles");
		listOfVoyelles = new JTextArea();
		entryListOfVoyelles = new LettersAndCommaJTextArea();
		radioVoyellesUsed = new JRadioButton("Ignore");
		resetVoyelles = new JButton("Reset");
		
		entryListOfVoyelles.getDocument().addDocumentListener(new DocumentListener()
		{
			
			public void removeUpdate(DocumentEvent e)
			{
				// TODO Auto-generated method stub
				GenerateSyllables.voyelles = setListFromEntry(entryListOfVoyelles);
				fillVoyelles();
			}
			
			public void insertUpdate(DocumentEvent e)
			{
				// TODO Auto-generated method stub
				GenerateSyllables.voyelles = setListFromEntry(entryListOfVoyelles);
				fillVoyelles();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// TODO Auto-generated method stub
				GenerateSyllables.voyelles = setListFromEntry(entryListOfVoyelles);
				fillVoyelles();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioVoyellesUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioVoyellesUsed.isSelected())
				{
					GenerateSyllables.voyelles = new String[]{};
					fillVoyelles();
				}
				else
				{
					setListFromEntry(entryListOfVoyelles);
				}
			}
		});
		
		resetVoyelles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.voyelles = GenerateSyllables.voyellesStatic;
				String stringOfVoyelles = fillVoyelles();
				entryListOfVoyelles.setText(stringOfVoyelles);
				radioVoyellesUsed.setSelected(false);
			}
		});
		listOfVoyelles.setEditable(false);
		entryListOfVoyelles.setEditable(true);	
		
		String stringOfVoyelles = fillVoyelles();
		entryListOfVoyelles.setText(stringOfVoyelles);
		
		generateSubPanelForSetOfLetters(c, 0, 0, labelListVoyelles, listOfVoyelles,
				entryListOfVoyelles, radioVoyellesUsed, resetVoyelles);
		
		labelConsonnes = new JLabel("Consonnes");
		listOfConsonnes = new JTextArea();
		entryListOfConsonnes = new LettersAndCommaJTextArea();		
		radioConsonnesUsed = new JRadioButton("Ignore");
		resetConsonnes = new JButton("Reset");
		
		entryListOfConsonnes.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				GenerateSyllables.consonnes = setListFromEntry(entryListOfConsonnes);
				fillConsonnes();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				GenerateSyllables.consonnes = setListFromEntry(entryListOfConsonnes);
				fillConsonnes();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				GenerateSyllables.consonnes = setListFromEntry(entryListOfConsonnes);
				fillConsonnes();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioConsonnesUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioConsonnesUsed.isSelected())
				{
					GenerateSyllables.consonnes = new String[]{};
					fillConsonnes();
				}
				else
				{
					setListFromEntry(entryListOfConsonnes);
				}
			}
		});
		
		resetConsonnes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.consonnes = GenerateSyllables.consonnesStatic;
				String stringOfConsonnes = fillConsonnes();
				entryListOfConsonnes.setText(stringOfConsonnes);
				radioConsonnesUsed.setSelected(false);
			}
		});
		
		listOfConsonnes.setEditable(false);
		entryListOfConsonnes.setEditable(true);	
		
		String stringOfConsonnes = fillConsonnes();
		entryListOfConsonnes.setText(stringOfConsonnes);
		
		generateSubPanelForSetOfLetters(c, 0, 1, labelConsonnes, listOfConsonnes,
				entryListOfConsonnes, radioConsonnesUsed, resetConsonnes);
		
		labelDoubleAndTripleVoyelles = new JLabel ("Double and triple voyelles");
		listOfDoubleAndTripleVoyelles = new JTextArea();
		entryListOfDoubleAndTripleVoyelles = new LettersAndCommaJTextArea();
		radioDoubleAndTripleVoyellesUsed = new JRadioButton("Ignore");
		resetDoubleAndTripleVoyelles = new JButton("Reset");
		
		entryListOfDoubleAndTripleVoyelles.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleVoyelles = setListFromEntry(entryListOfDoubleAndTripleVoyelles);
				fillDoubleAndTripleVoyelles();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleVoyelles = setListFromEntry(entryListOfDoubleAndTripleVoyelles);
				fillDoubleAndTripleVoyelles();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleVoyelles = setListFromEntry(entryListOfDoubleAndTripleVoyelles);
				fillDoubleAndTripleVoyelles();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioDoubleAndTripleVoyellesUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioDoubleAndTripleVoyellesUsed.isSelected())
				{
					GenerateSyllables.okDoubleVoyelles = new String[]{};
					fillDoubleAndTripleVoyelles();
				}
				else
				{
					setListFromEntry(entryListOfDoubleAndTripleVoyelles);
				}
			}
		});
		
		resetDoubleAndTripleVoyelles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okDoubleVoyelles = GenerateSyllables.okDoubleVoyellesStatic;
				String stringOfDoubleAndTripleVoyelles = fillDoubleAndTripleVoyelles();
				entryListOfDoubleAndTripleVoyelles.setText(stringOfDoubleAndTripleVoyelles);
				radioDoubleAndTripleVoyellesUsed.setSelected(false);
			}
		});
		
		listOfDoubleAndTripleVoyelles.setEditable(false);
		entryListOfDoubleAndTripleVoyelles.setEditable(true);	
		
		String stringOfDoubleAndTripleVoyelles = fillDoubleAndTripleVoyelles();
		entryListOfDoubleAndTripleVoyelles.setText(stringOfDoubleAndTripleVoyelles);
		
		generateSubPanelForSetOfLetters(c, 0, 2, labelDoubleAndTripleVoyelles, listOfDoubleAndTripleVoyelles,
				entryListOfDoubleAndTripleVoyelles, radioDoubleAndTripleVoyellesUsed, resetDoubleAndTripleVoyelles);
		
		
		labelDoubleConsonnesMiddle = new JLabel("Double consonnes middle place");
		listOfDoubleConsonnesMiddle = new JTextArea();
		entryListOfDoubleConsonnesMiddle = new LettersAndCommaJTextArea();
		
		radioDoubleConsonnesMiddleUsed = new JRadioButton("Ignore");
		resetDoubleConsonnesMiddle = new JButton("Reset");
		
		entryListOfDoubleConsonnesMiddle.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnes = setListFromEntry(entryListOfDoubleConsonnesMiddle);
				fillDoubleConsonnesMiddle();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnes = setListFromEntry(entryListOfDoubleConsonnesMiddle);
				fillDoubleConsonnesMiddle();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnes = setListFromEntry(entryListOfDoubleConsonnesMiddle);
				fillDoubleConsonnesMiddle();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioDoubleConsonnesMiddleUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioDoubleConsonnesMiddleUsed.isSelected())
				{
					GenerateSyllables.okDoubleConsonnes = new String[]{};
					fillDoubleConsonnesMiddle();
				}
				else
				{
					setListFromEntry(entryListOfDoubleConsonnesMiddle);
				}
			}
		});
		
		resetDoubleConsonnesMiddle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okDoubleConsonnes = GenerateSyllables.okDoubleConsonnesStatic;
				String stringOfDoubleConsonnesMiddle = fillDoubleConsonnesMiddle();
				entryListOfDoubleConsonnesMiddle.setText(stringOfDoubleConsonnesMiddle);
				radioDoubleConsonnesMiddleUsed.setSelected(false);
			}
		});
		
		listOfDoubleConsonnesMiddle.setEditable(false);
		entryListOfDoubleConsonnesMiddle.setEditable(true);	
		
		String stringOfDoubleConsonnesMiddle = fillDoubleConsonnesMiddle();
		entryListOfDoubleConsonnesMiddle.setText(stringOfDoubleConsonnesMiddle);
		
		generateSubPanelForSetOfLetters(c, 0, 3, labelDoubleConsonnesMiddle, listOfDoubleConsonnesMiddle,
				entryListOfDoubleConsonnesMiddle, radioDoubleConsonnesMiddleUsed, resetDoubleConsonnesMiddle);
		
		labelTripleConsonnesMiddle = new JLabel("Triple consonnes middle place");
		listOfTripleConsonnesMiddle = new JTextArea();
		entryListOfTripleConsonnesMiddle = new LettersAndCommaJTextArea();
		
		radioTripleConsonnesMiddleUsed = new JRadioButton("Ignore");
		resetTripleConsonnesMiddle = new JButton("Reset");
		
		entryListOfTripleConsonnesMiddle.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okTripleConsonnes = setListFromEntry(entryListOfTripleConsonnesMiddle);
				fillTripleConsonnesMiddle();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okTripleConsonnes = setListFromEntry(entryListOfTripleConsonnesMiddle);
				fillTripleConsonnesMiddle();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okTripleConsonnes = setListFromEntry(entryListOfTripleConsonnesMiddle);
				fillTripleConsonnesMiddle();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioTripleConsonnesMiddleUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioTripleConsonnesMiddleUsed.isSelected())
				{
					GenerateSyllables.okTripleConsonnes = new String[]{};
					fillTripleConsonnesMiddle();
				}
				else
				{
					setListFromEntry(entryListOfTripleConsonnesMiddle);
				}
			}
		});
		
		resetTripleConsonnesMiddle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okTripleConsonnes = GenerateSyllables.okTripleConsonnesStatic;
				String stringOfTripleConsonnesMiddle = fillTripleConsonnesMiddle();
				entryListOfTripleConsonnesMiddle.setText(stringOfTripleConsonnesMiddle);
				radioTripleConsonnesMiddleUsed.setSelected(false);
			}
		});
		
		listOfTripleConsonnesMiddle.setEditable(false);
		entryListOfTripleConsonnesMiddle.setEditable(true);	
		
		String stringOfTripleConsonnesMiddle = fillTripleConsonnesMiddle();
		entryListOfTripleConsonnesMiddle.setText(stringOfTripleConsonnesMiddle);
		
		generateSubPanelForSetOfLetters(c, 0, 4, labelTripleConsonnesMiddle, listOfTripleConsonnesMiddle,
				entryListOfTripleConsonnesMiddle, radioTripleConsonnesMiddleUsed, resetTripleConsonnesMiddle);
		
		
		labelDoubleAndTripleVoyellesDebut = new JLabel("Double and triple voyelles at beginning");
		listOfDoubleAndTripleVoyellesDebut = new JTextArea();
		entryListOfDoubleAndTripleVoyellesDebut = new LettersAndCommaJTextArea();
			
		radioDoubleAndTripleVoyellesDebutUsed = new JRadioButton("Ignore");
		resetDoubleAndTripleVoyellesDebut = new JButton("Reset");
		
		entryListOfDoubleAndTripleVoyellesDebut.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleVoyellesDebut = setListFromEntry(entryListOfDoubleAndTripleVoyellesDebut);
				fillDoubleAndTripleVoyellesDebut();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleVoyellesDebut = setListFromEntry(entryListOfDoubleAndTripleVoyellesDebut);
				fillDoubleAndTripleVoyellesDebut();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleVoyellesDebut = setListFromEntry(entryListOfDoubleAndTripleVoyellesDebut);
				fillDoubleAndTripleVoyellesDebut();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioDoubleAndTripleVoyellesDebutUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioDoubleAndTripleVoyellesDebutUsed.isSelected())
				{
					GenerateSyllables.okDoubleVoyellesDebut = new String[]{};
					fillDoubleAndTripleVoyellesDebut();
				}
				else
				{
					setListFromEntry(entryListOfDoubleAndTripleVoyellesDebut);
				}
			}
		});
		
		resetDoubleAndTripleVoyellesDebut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okDoubleVoyellesDebut = GenerateSyllables.okDoubleVoyellesDebutStatic;
				String stringOfDoubleAndTripleVoyellesDebut = fillDoubleAndTripleVoyellesDebut();
				entryListOfDoubleAndTripleVoyellesDebut.setText(stringOfDoubleAndTripleVoyellesDebut);
				radioDoubleAndTripleVoyellesDebutUsed.setSelected(false);
			}
		});
		
		listOfDoubleAndTripleVoyellesDebut.setEditable(false);
		entryListOfDoubleAndTripleVoyellesDebut.setEditable(true);	
		
		String stringOfDoubleAndTripleVoyellesDebut = fillDoubleAndTripleVoyellesDebut();
		entryListOfDoubleAndTripleVoyellesDebut.setText(stringOfDoubleAndTripleVoyellesDebut);
		
		generateSubPanelForSetOfLetters(c, 1, 0, labelDoubleAndTripleVoyellesDebut, listOfDoubleAndTripleVoyellesDebut,
				entryListOfDoubleAndTripleVoyellesDebut, radioDoubleAndTripleVoyellesDebutUsed, resetDoubleAndTripleVoyellesDebut);
		
		
		labelDoubleConsonnesDebut = new JLabel("Double consonnes at beginning");
		listOfDoubleConsonnesDebut = new JTextArea();
		entryListOfDoubleConsonnesDebut = new LettersAndCommaJTextArea();
		
		radioDoubleConsonnesDebutUsed = new JRadioButton("Ignore");
		resetDoubleConsonnesDebut = new JButton("Reset");
		
		entryListOfDoubleConsonnesDebut.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnesDebut = setListFromEntry(entryListOfDoubleConsonnesDebut);
				fillDoubleConsonnesDebut();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnesDebut = setListFromEntry(entryListOfDoubleConsonnesDebut);
				fillDoubleConsonnesDebut();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnesDebut = setListFromEntry(entryListOfDoubleConsonnesDebut);
				fillDoubleConsonnesDebut();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioDoubleConsonnesDebutUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioDoubleConsonnesDebutUsed.isSelected())
				{
					GenerateSyllables.okDoubleConsonnesDebut = new String[]{};
					fillDoubleConsonnesDebut();
				}
				else
				{
					setListFromEntry(entryListOfDoubleConsonnesDebut);
				}
			}
		});
		
		resetDoubleConsonnesDebut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okDoubleConsonnesDebut = GenerateSyllables.okDoubleConsonnesDebutStatic;
				String stringOfDoubleConsonnesDebut = fillDoubleConsonnesDebut();
				entryListOfDoubleConsonnesDebut.setText(stringOfDoubleConsonnesDebut);
				radioDoubleConsonnesDebutUsed.setSelected(false);
			}
		});
		
		listOfDoubleConsonnesDebut.setEditable(false);
		entryListOfDoubleConsonnesDebut.setEditable(true);	
		
		String stringOfDoubleConsonnesDebut = fillDoubleConsonnesDebut();
		entryListOfDoubleConsonnesDebut.setText(stringOfDoubleConsonnesDebut);
		
		generateSubPanelForSetOfLetters(c, 1, 1, labelDoubleConsonnesDebut, listOfDoubleConsonnesDebut,
				entryListOfDoubleConsonnesDebut, radioDoubleConsonnesDebutUsed, resetDoubleConsonnesDebut);
		
		
		labelTripleConsonnesDebut = new JLabel("Triple consonnes at beginning");
		listOfTripleConsonnesDebut = new JTextArea();
		entryListOfTripleConsonnesDebut = new LettersAndCommaJTextArea();
			
		radioTripleConsonnesDebutUsed = new JRadioButton("Ignore");
		resetTripleConsonnesDebut = new JButton("Reset");
		
		entryListOfTripleConsonnesDebut.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okTripleConsonnesDebut = setListFromEntry(entryListOfTripleConsonnesDebut);
				fillTripleConsonnesDebut();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioTripleConsonnesDebutUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioTripleConsonnesDebutUsed.isSelected())
				{
					GenerateSyllables.okTripleConsonnesDebut = new String[]{};
					fillTripleConsonnesDebut();
				}
				else
				{
					setListFromEntry(entryListOfTripleConsonnesDebut);
				}
			}
		});
		
		resetTripleConsonnesDebut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okTripleConsonnesDebut = GenerateSyllables.okTripleConsonnesDebutStatic;
				String stringOfTripleConsonnesDebut = fillTripleConsonnesDebut();
				entryListOfTripleConsonnesDebut.setText(stringOfTripleConsonnesDebut);
				radioTripleConsonnesDebutUsed.setSelected(false);
			}
		});
		
		listOfTripleConsonnesDebut.setEditable(false);
		entryListOfTripleConsonnesDebut.setEditable(true);	
		
		String stringOfTripleConsonnesDebut = fillTripleConsonnesDebut();
		entryListOfTripleConsonnesDebut.setText(stringOfTripleConsonnesDebut);
		
		generateSubPanelForSetOfLetters(c, 1, 2, labelTripleConsonnesDebut, listOfTripleConsonnesDebut,
				entryListOfTripleConsonnesDebut, radioTripleConsonnesDebutUsed, resetTripleConsonnesDebut);
		
		labelDoubleConsonnesFin = new JLabel("Double consonnes at the end");
		listOfDoubleConsonnesFin = new JTextArea();
		entryListOfDoubleConsonnesFin = new LettersAndCommaJTextArea();
		
		radioDoubleConsonnesFinUsed = new JRadioButton("Ignore");
		resetDoubleConsonnesFin = new JButton("Reset");
		
		entryListOfDoubleConsonnesFin.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okDoubleConsonnesEnd = setListFromEntry(entryListOfDoubleConsonnesFin);
				fillDoubleConsonnesFin();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioDoubleConsonnesFinUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioDoubleConsonnesFinUsed.isSelected())
				{
					GenerateSyllables.okDoubleConsonnesEnd = new String[]{};
					fillDoubleConsonnesFin();
				}
				else
				{
					setListFromEntry(entryListOfDoubleConsonnesFin);
				}
			}
		});
		
		resetDoubleConsonnesFin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okDoubleConsonnesEnd = GenerateSyllables.okDoubleConsonnesEndStatic;
				String stringOfDoubleConsonnesFin  = fillDoubleConsonnesFin ();
				entryListOfDoubleConsonnesFin .setText(stringOfDoubleConsonnesFin );
				radioDoubleConsonnesFinUsed.setSelected(false);
			}
		});
		
		listOfDoubleConsonnesFin.setEditable(false);
		entryListOfDoubleConsonnesFin.setEditable(true);	
		
		String stringOfDoubleConsonnesFin  = fillDoubleConsonnesFin ();
		entryListOfDoubleConsonnesFin .setText(stringOfDoubleConsonnesFin );
		
		generateSubPanelForSetOfLetters(c, 1, 3, labelDoubleConsonnesFin, listOfDoubleConsonnesFin,
				entryListOfDoubleConsonnesFin, radioDoubleConsonnesFinUsed, resetDoubleConsonnesFin);
		
		
		labelTripleConsonnesFin = new JLabel("Triple consonnes at the end");
		listOfTripleConsonnesFin = new JTextArea();
		entryListOfTripleConsonnesFin = new LettersAndCommaJTextArea();
			
		radioTripleConsonnesFinUsed = new JRadioButton("Ignore");
		resetTripleConsonnesFin = new JButton("Reset");
		
		entryListOfTripleConsonnesFin.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// Try to parse to update the list. Maybe limit to ENTER or other.
				GenerateSyllables.okTripleConsonnesEnd = setListFromEntry(entryListOfTripleConsonnesFin);
				fillTripleConsonnesFin();
			}
		});
		
		// Ignore might be static, when generating, use or not this part...
		radioTripleConsonnesFinUsed.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (radioTripleConsonnesFinUsed.isSelected())
				{
					GenerateSyllables.okTripleConsonnesEnd = new String[]{};
					fillTripleConsonnesFin();
				}
				else
				{
					setListFromEntry(entryListOfTripleConsonnesFin);
				}
			}
		});
		
		resetTripleConsonnesFin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Reset
				GenerateSyllables.okTripleConsonnesEnd = GenerateSyllables.okTripleConsonnesEndStatic;
				String stringOfTripleConsonnesFin = fillTripleConsonnesFin();
				entryListOfTripleConsonnesFin.setText(stringOfTripleConsonnesFin);
				radioTripleConsonnesFinUsed.setSelected(false);
			}
		});
		
		listOfTripleConsonnesFin.setEditable(false);
		entryListOfTripleConsonnesFin.setEditable(true);	
		
		String stringOfTripleConsonnesFin = fillTripleConsonnesFin();
		entryListOfTripleConsonnesFin.setText(stringOfTripleConsonnesFin);
		
		generateSubPanelForSetOfLetters(c, 1, 4, labelTripleConsonnesFin, listOfTripleConsonnesFin,
				entryListOfTripleConsonnesFin, radioTripleConsonnesFinUsed, resetTripleConsonnesFin);
		
		// Wanted combinations
		labelCombinations = new JLabel("Wanted combinations");
		listOfCombinations = new JList();
		entryListOfCombinations = new JTextArea();
		entryListOfCombinations.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				//
			}
		});
		
		JPanel combinationsPanel = new JPanel();
		combinationsPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints cSub= new GridBagConstraints();
		
		cSub.fill = GridBagConstraints.BOTH;
		cSub.gridx = 0;
		cSub.gridy = 0;
		cSub.gridwidth = 1;
		combinationsPanel.add(labelCombinations,cSub);
		
		GridBagConstraints cSub2= new GridBagConstraints();
		cSub2.fill = GridBagConstraints.BOTH;
		cSub2.gridx = 0;
		cSub2.gridy = 1;
		cSub2.gridwidth = 1;
		combinationsPanel.add(listOfCombinations,cSub2);
		
		GridBagConstraints cSub3= new GridBagConstraints();
		cSub3.fill = GridBagConstraints.BOTH;
		cSub3.gridx = 0;
		cSub3.gridy = 2;
		cSub3.gridwidth = 1;
		combinationsPanel.add(entryListOfCombinations,cSub3);
			
		GridBagConstraints c2= new GridBagConstraints();
		c2.insets = new Insets(0, 0, 5, 5);
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 5;
		c2.gridwidth = 1;
		getContentPane().add(combinationsPanel, c2);
		
		// Black list, sequence of letters we never want to have
		labelBlackList = new JLabel("Black list");
		blackList = new JList();
		entryBlackList = new LettersAndCommaJTextArea();
		fillBlackList();
		
		entryBlackList.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				// TODO Auto-generated method stub
				GenerateNames.blackList = setListFromEntry(entryBlackList);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				// TODO Auto-generated method stub
				GenerateNames.blackList = setListFromEntry(entryBlackList);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				// TODO Auto-generated method stub
				GenerateNames.blackList = setListFromEntry(entryListOfVoyelles);
			}
		});
		
		JPanel blackListPanel = new JPanel();
		blackListPanel.setLayout(new GridBagLayout());
			
		GridBagConstraints cSub4= new GridBagConstraints();
		cSub4.fill = GridBagConstraints.BOTH;
		cSub4.gridx = 0;
		cSub4.gridy = 0;
		cSub4.gridwidth = 1;
		blackListPanel.add(labelBlackList,cSub4);
		
		GridBagConstraints cSub5= new GridBagConstraints();
		cSub5.fill = GridBagConstraints.BOTH;
		cSub5.gridx = 0;
		cSub5.gridy = 1;
		cSub5.gridwidth = 1;
		blackListPanel.add(blackList,cSub5);
		
		GridBagConstraints cSub6= new GridBagConstraints();
		cSub6.fill = GridBagConstraints.BOTH;
		cSub6.gridx = 0;
		cSub6.gridy = 2;
		cSub6.gridwidth = 1;
		blackListPanel.add(entryBlackList,cSub6);
			
//		GridBagConstraints cSub= new GridBagConstraints();
		GridBagConstraints c3= new GridBagConstraints();
		c3.insets = new Insets(0, 0, 5, 5);
		c3.fill = GridBagConstraints.BOTH;
		c3.gridx = 1;
		c3.gridy = 5;
		c3.gridwidth = 1;
		getContentPane().add(blackListPanel, c3);
		
		// Add the choice of renderers

		GridBagConstraints c35= new GridBagConstraints();
		c35.insets = new Insets(5, 5, 5, 5);
		c35.fill = GridBagConstraints.BOTH;
		c35.gridx = 2;
		c35.gridy = 2;

		JPanel renderersPanels = new JPanel();
		BoxLayout renderersLayout = new BoxLayout(renderersPanels, BoxLayout.Y_AXIS);
		renderersPanels.setLayout(renderersLayout);
		// Scan the list of renderers
		// Add the choice
		ArrayList<IRenderer> allRenderer = RenderersScanner.getAllCurrentRenderers();
		
		boolean isFirst = true; // Select the first renderer at least
		
		for (IRenderer oneRenderer : allRenderer)
		{
			JToggleButton radioOneRenderer = new JToggleButton(oneRenderer.getNameForUI());
			radioOneRenderer.addChangeListener(new ChangeListener()
			{
				
				@Override
				public void stateChanged(ChangeEvent e)
				{
					if (radioOneRenderer.isSelected())
					{
						// Inform the generator it needs to use this renderer
						GenerateNames.addARenderer(oneRenderer);
					}
					else
					{
						GenerateNames.removeARenderer(oneRenderer);
					}
				}
			});
			renderersPanels.add(radioOneRenderer);
			if (isFirst)
			{
				radioOneRenderer.doClick();
				isFirst = false;
			}
		}
		
		getContentPane().add(renderersPanels,c35);
		
//		doDeleteButton = new JToggleButton("Delete");
		// Generate button
		GridBagConstraints c4= new GridBagConstraints();
		c4.insets = new Insets(0, 0, 5, 0);
		
		generateButton = new JButton("Generate");
		generateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if (generateButton.getText().equals("Generate"))
				{
					GenerateNames.resume();
					GenerateNames aNewGenerator = new GenerateNames();
					Thread workingThread = new Thread(aNewGenerator);
					workingThread.start();
					generateButton.setText("STOP");
				}
				else
				{
					GenerateNames.stop();
				}
			}
		});
		c4.fill = GridBagConstraints.BOTH;
		c4.gridx = 2;
		c4.gridy = 3;
		getContentPane().add(generateButton,c4);
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 2;
		gbc_panel.gridy = 5;
		getContentPane().add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblMinimumNumberOf = new JLabel("Minimum number of letters");
		GridBagConstraints gbc_lblMinimumNumberOf = new GridBagConstraints();
		gbc_lblMinimumNumberOf.insets = new Insets(0, 0, 5, 0);
		gbc_lblMinimumNumberOf.gridx = 0;
		gbc_lblMinimumNumberOf.gridy = 0;
		panel.add(lblMinimumNumberOf, gbc_lblMinimumNumberOf);
		
		lblMaximumNumberOf = new JLabel("Maximum number of letters");
		GridBagConstraints gbc_lblMaximumNumberOf = new GridBagConstraints();
		gbc_lblMaximumNumberOf.gridx = 0;
		gbc_lblMaximumNumberOf.gridy = 1;
		panel.add(lblMaximumNumberOf, gbc_lblMaximumNumberOf);
		
		entryMinimumNumberOf = new IntegerJTextField("4");
		entryMinimumNumberOf.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				System.out.println("Setting min number to "+entryMinimumNumberOf.getText());
				GenerateNames.setMinNumberOfLetters(entryMinimumNumberOf.getText());
			}
		});
		
		GridBagConstraints gbc_entryMinimumNumberOf = new GridBagConstraints();
		gbc_entryMinimumNumberOf.insets = new Insets(0, 0, 5, 0);
		gbc_entryMinimumNumberOf.gridx = 1;
		gbc_entryMinimumNumberOf.gridy = 0;
		panel.add(entryMinimumNumberOf, gbc_entryMinimumNumberOf);
		
		entryMaximumNumberOf = new IntegerJTextField("15");
		GridBagConstraints gbc_entryMaximumNumberOf = new GridBagConstraints();
		gbc_entryMaximumNumberOf.gridx = 1;
		gbc_entryMaximumNumberOf.gridy = 1;
		panel.add(entryMaximumNumberOf, gbc_entryMaximumNumberOf);
		entryMaximumNumberOf.getDocument().addDocumentListener(new DocumentListener()
		{
			
			@Override
			public void removeUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e)
			{
				System.out.println("Setting max number to "+entryMaximumNumberOf.getText());
				GenerateNames.setMaxNumberOfLetters(entryMaximumNumberOf.getText());
			}
		});
		
		infoArea = new JTextArea("Information");
		nbOfCombinationsInfo = new JTextArea("Waiting generation");
		
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridBagLayout());
			
		GridBagConstraints cSub7= new GridBagConstraints();
		
		cSub7.fill = GridBagConstraints.BOTH;
		cSub7.gridx = 0;
		cSub7.gridy = 0;
		cSub7.gridwidth = 1;
		infoPanel.add(infoArea,cSub7);
		
		GridBagConstraints cSub8= new GridBagConstraints();
		cSub8.fill = GridBagConstraints.BOTH;
		cSub8.gridx = 0;
		cSub8.gridy = 1;
		cSub8.gridwidth = 1;
		infoPanel.add(nbOfCombinationsInfo,cSub8);
			
		GridBagConstraints c5= new GridBagConstraints();
		c5.insets = new Insets(0, 0, 0, 5);
		
		c5.fill = GridBagConstraints.BOTH;
		c5.gridx = 0;
		c5.gridy = 6;
		c5.gridwidth = 2;
		getContentPane().add(infoPanel, c5);
		
		
		
		this.pack();
		this.setSize(1100, 820);
		
		  // Set the default close operation for the window, or else the
        // program won't exit when clicking close button
        //  (The default is HIDE_ON_CLOSE, which just makes the window
        //  invisible, and thus doesn't exit the app)
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        mnFile = new JMenu("File");
        menuBar.add(mnFile);
        
        mntmOpen = new JMenuItem("Open");
        
        mntmOpen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) 
        	{
        		 loadDialog();
        	}
        });
       
        mnFile.add(mntmOpen);
        
        mntmSave = new JMenuItem("Save");
        mntmSave.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) 
        	{
        		saveDialog();
        	}
        });
        mnFile.add(mntmSave);
        
        mntmSaveSettings = new JMenuItem("Save settings");
        mntmSaveSettings.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) 
        	{
        		 saveAutomatic();
        	}
        });
       
        menuBar.add(mntmSaveSettings);
        
        mntmOpenSettings = new JMenuItem("Open settings");
        mntmOpenSettings.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) 
        	{
        		 loadAutomatic();
        	}
        });
        menuBar.add(mntmOpenSettings);
        
        mnAbout = new JMenu("About");
        menuBar.add(mnAbout);

        // Set the visibility as true, thereby displaying it
        this.setVisible(true);
	}

	protected void saveDialog()
	{
		//Create a file chooser
		final JFileChooser fc = new JFileChooser();

		fc.setFileFilter(new NameSaveFilter());
		//In response to a button click:
		int returnVal = fc.showOpenDialog(this);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) 
		{
            File file = fc.getSelectedFile();
            
            File renamedFile = new File(file.getAbsolutePath()+".sav");
            //This is where a real application would open the file.
           logger.info("Will save in {}",renamedFile);
           System.out.println("Will save in "+renamedFile);
           
           LoadSaveConfig myNewNewLSC = new LoadSaveConfig();
           
           myNewNewLSC.saveCurrentConfigToFile(renamedFile);
           
           infoArea.setText("Settings saved in "+renamedFile.getName());
        } 
		else 
		{
        	logger.info("Open command cancelled by user.");
        }

	}
	
	protected void loadDialog()
	{
		//Create a file chooser
		final JFileChooser fc = new JFileChooser();

		fc.setFileFilter(new NameSaveFilter());
		//In response to a button click:
		int returnVal = fc.showOpenDialog(this);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) 
		{
            File file = fc.getSelectedFile();
            //This is where a real application would open the file.
           logger.info("Will load from {}",file);
           
           LoadSaveConfig myNewNewLSC = new LoadSaveConfig();
           
           myNewNewLSC.loadConfigFromFile(file);
           
           fillFieldsFromGenerateSyllables();
           
           infoArea.setText("Settings loaded from "+file.getName());
        } 
		else 
		{
        	logger.info("Open command cancelled by user.");
        }

	}
	
	/**
	 *  Save a default configuration
	 */
	protected void saveAutomatic()
	{
		
           logger.info("Will save in the default save file");
           
           LoadSaveConfig myNewNewLSC = new LoadSaveConfig();
           
           myNewNewLSC.saveCurrentConfigToFile(null);
           
           if (myNewNewLSC.getFileUsed() != null)
           {
        	   infoArea.setText("Settings saved in "+myNewNewLSC.getFileUsed().getName());
           }
           else
           {
        	   infoArea.setText("Settings saved in default configuration file");
           }
	}
	
	/**
	 *  Load a default configuration
	 */
	protected void loadAutomatic()
	{
		File rootFolder = new File("saveParameters/");
		
		File saveFiles[] = rootFolder.listFiles(new NameSaveFilter());
		
		long lastMod = Long.MIN_VALUE;
        File choise = null;
        for (File file : saveFiles) {
                if (file.lastModified() > lastMod) {
                        choise = file;
                        lastMod = file.lastModified();
                }
        }

        if (choise != null)
        {

        	//This is where a real application would open the file.
        	logger.info("Will load from {}",choise);

        	LoadSaveConfig myNewNewLSC = new LoadSaveConfig();

        	myNewNewLSC.loadConfigFromFile(choise);

        	fillFieldsFromGenerateSyllables();
        	
        	infoArea.setText("Settings loaded from "+choise.getName());
        }
	}

	private void fillFieldsFromGenerateSyllables()
	{
		String stringOfVoyelles = fillVoyelles();
		entryListOfVoyelles.setText(stringOfVoyelles);

		String stringOfConsonnes = fillConsonnes();
		entryListOfConsonnes.setText(stringOfConsonnes);
		
		String stringOfDoubleAndTripleVoyelles = fillDoubleAndTripleVoyelles();
		entryListOfDoubleAndTripleVoyelles.setText(stringOfDoubleAndTripleVoyelles);
		
		String stringOfDoubleConsonnesMiddle = fillDoubleConsonnesMiddle();
		entryListOfDoubleConsonnesMiddle.setText(stringOfDoubleConsonnesMiddle);
		
		String stringOfTripleConsonnesMiddle = fillTripleConsonnesMiddle();
		entryListOfTripleConsonnesMiddle.setText(stringOfTripleConsonnesMiddle);
		
		String stringOfDoubleAndTripleVoyellesDebut = fillDoubleAndTripleVoyellesDebut();
		entryListOfDoubleAndTripleVoyellesDebut.setText(stringOfDoubleAndTripleVoyellesDebut);
		
		String stringOfDoubleConsonnesDebut = fillDoubleConsonnesDebut();
		entryListOfDoubleConsonnesDebut.setText(stringOfDoubleConsonnesDebut);
		
		String stringOfTripleConsonnesDebut = fillTripleConsonnesDebut();
		entryListOfTripleConsonnesDebut.setText(stringOfTripleConsonnesDebut);
		
		String stringOfDoubleConsonnesFin  = fillDoubleConsonnesFin ();
		entryListOfDoubleConsonnesFin .setText(stringOfDoubleConsonnesFin );
		
		String stringOfTripleConsonnesFin = fillTripleConsonnesFin();
		entryListOfTripleConsonnesFin.setText(stringOfTripleConsonnesFin);
		
		fillBlackList();
	}
	
	protected String[] setListFromEntry(LettersAndCommaJTextArea entryListOfVoyelles2)
	{
//		System.out.println("Splitting from "+entryListOfVoyelles2.getText());
		String stringOfElements = entryListOfVoyelles2.getText().replace(" ", "");
		stringOfElements = stringOfElements.replace("\n", "");
//		System.out.println("2 - Splitting from "+stringOfElements);
		String[] splittedString = stringOfElements.split(",");
		
		return splittedString;
	}

	/**
	 * @return
	 */
	private String fillVoyelles()
	{
		String stringOfVoyelles="";
		for (int iVoyelles = 0; iVoyelles < GenerateSyllables.voyelles.length-1 ; iVoyelles++)
		{
			stringOfVoyelles+=GenerateSyllables.voyelles[iVoyelles];
			stringOfVoyelles+=", ";
			
			if ((iVoyelles + 1 ) % 20 == 0)
			{
				stringOfVoyelles+="\n";
			}
		}
		if (GenerateSyllables.voyelles.length != 0)
		{
			stringOfVoyelles+=GenerateSyllables.voyelles[GenerateSyllables.voyelles.length-1];
		}
		
		listOfVoyelles.setText(stringOfVoyelles);
		return stringOfVoyelles;
	}

	private String fillConsonnes()
	{
		String stringOfConsonnes="";
		for (int iConsonnes = 0; iConsonnes < GenerateSyllables.consonnes.length-1 ; iConsonnes++)
		{
			stringOfConsonnes+=GenerateSyllables.consonnes[iConsonnes];
			stringOfConsonnes+=", ";
			
			if ((iConsonnes + 1) % 20 == 0)
			{
				stringOfConsonnes+="\n";
			}
		}
		if (GenerateSyllables.consonnes.length != 0)
		{
			stringOfConsonnes+=GenerateSyllables.consonnes[GenerateSyllables.consonnes.length-1];
		}
		
		listOfConsonnes.setText(stringOfConsonnes);
		return stringOfConsonnes;
	}
	
	private String fillDoubleAndTripleVoyelles()
	{
		String stringOfDoubleAndTripleVoyelles="";
		for (int iDoubleAndTripleVoyelles = 0; iDoubleAndTripleVoyelles < GenerateSyllables.okDoubleVoyelles.length-1 ; iDoubleAndTripleVoyelles++)
		{
			stringOfDoubleAndTripleVoyelles+=GenerateSyllables.okDoubleVoyelles[iDoubleAndTripleVoyelles];
			stringOfDoubleAndTripleVoyelles+=", ";
			
			if ((iDoubleAndTripleVoyelles + 1) % 20 == 0)
			{
				stringOfDoubleAndTripleVoyelles+="\n";
			}
		}
		if (GenerateSyllables.okDoubleVoyelles.length != 0)
		{
			stringOfDoubleAndTripleVoyelles+=GenerateSyllables.okDoubleVoyelles[GenerateSyllables.okDoubleVoyelles.length-1];
		}
		
		listOfDoubleAndTripleVoyelles.setText(stringOfDoubleAndTripleVoyelles);
		return stringOfDoubleAndTripleVoyelles;
	}
	
	private String fillDoubleConsonnesMiddle()
	{
		String stringOfDoubleConsonnesMiddle="";
		for (int iDoubleConsonnesMiddle = 0; iDoubleConsonnesMiddle < GenerateSyllables.okDoubleConsonnes.length-1 ; iDoubleConsonnesMiddle++)
		{
			stringOfDoubleConsonnesMiddle+=GenerateSyllables.okDoubleConsonnes[iDoubleConsonnesMiddle];
			stringOfDoubleConsonnesMiddle+=", ";
			
			if ((iDoubleConsonnesMiddle + 1) % 20 == 0)
			{
				stringOfDoubleConsonnesMiddle+="\n";
			}
		}
		if (GenerateSyllables.okDoubleConsonnes.length != 0)
		{
			stringOfDoubleConsonnesMiddle+=GenerateSyllables.okDoubleConsonnes[GenerateSyllables.okDoubleConsonnes.length-1];
		}
		
		listOfDoubleConsonnesMiddle.setText(stringOfDoubleConsonnesMiddle);
		return stringOfDoubleConsonnesMiddle;
	}
	
	private String fillTripleConsonnesMiddle()
	{
		String stringOfTripleConsonnesMiddle="";
		for (int iTripleConsonnesMiddle = 0; iTripleConsonnesMiddle < GenerateSyllables.okTripleConsonnes.length-1 ; iTripleConsonnesMiddle++)
		{
			stringOfTripleConsonnesMiddle+=GenerateSyllables.okTripleConsonnes[iTripleConsonnesMiddle];
			stringOfTripleConsonnesMiddle+=", ";
			
			if ((iTripleConsonnesMiddle + 1) % 20 == 0)
			{
				stringOfTripleConsonnesMiddle+="\n";
			}
		}
		if (GenerateSyllables.okTripleConsonnes.length != 0)
		{
			stringOfTripleConsonnesMiddle+=GenerateSyllables.okTripleConsonnes[GenerateSyllables.okTripleConsonnes.length-1];
		}
		
		listOfTripleConsonnesMiddle.setText(stringOfTripleConsonnesMiddle);
		return stringOfTripleConsonnesMiddle;
	}
	
	private String fillDoubleAndTripleVoyellesDebut()
	{
		String stringOfDoubleAndTripleVoyellesDebut="";
		for (int iDoubleAndTripleVoyellesDebut = 0; iDoubleAndTripleVoyellesDebut < GenerateSyllables.okDoubleVoyellesDebut.length-1 ; iDoubleAndTripleVoyellesDebut++)
		{
			stringOfDoubleAndTripleVoyellesDebut+=GenerateSyllables.okDoubleVoyellesDebut[iDoubleAndTripleVoyellesDebut];
			stringOfDoubleAndTripleVoyellesDebut+=", ";
			
			if ((iDoubleAndTripleVoyellesDebut + 1) % 20 == 0)
			{
				stringOfDoubleAndTripleVoyellesDebut+="\n";
			}
		}
		if (GenerateSyllables.okDoubleVoyellesDebut.length != 0)
		{
			stringOfDoubleAndTripleVoyellesDebut+=GenerateSyllables.okDoubleVoyellesDebut[GenerateSyllables.okDoubleVoyellesDebut.length-1];
		}
		
		listOfDoubleAndTripleVoyellesDebut.setText(stringOfDoubleAndTripleVoyellesDebut);
		return stringOfDoubleAndTripleVoyellesDebut;
	}
	
	private String fillDoubleConsonnesDebut()
	{
		String stringOfDoubleConsonnesDebut="";
		for (int iDoubleConsonnesDebut = 0; iDoubleConsonnesDebut < GenerateSyllables.okDoubleConsonnesDebut.length-1 ; iDoubleConsonnesDebut++)
		{
			stringOfDoubleConsonnesDebut+=GenerateSyllables.okDoubleConsonnesDebut[iDoubleConsonnesDebut];
			stringOfDoubleConsonnesDebut+=", ";
			
			if ((iDoubleConsonnesDebut + 1) % 20 == 0)
			{
				stringOfDoubleConsonnesDebut+="\n";
			}
		}
		if (GenerateSyllables.okDoubleConsonnesDebut.length != 0)
		{
			stringOfDoubleConsonnesDebut+=GenerateSyllables.okDoubleConsonnesDebut[GenerateSyllables.okDoubleConsonnesDebut.length-1];
		}
		
		listOfDoubleConsonnesDebut.setText(stringOfDoubleConsonnesDebut);
		return stringOfDoubleConsonnesDebut;
	}
	
	private String fillTripleConsonnesDebut()
	{
		String stringOfTripleConsonnesDebut="";
		for (int iTripleConsonnesDebut = 0; iTripleConsonnesDebut < GenerateSyllables.okTripleConsonnesDebut.length-1 ; iTripleConsonnesDebut++)
		{
			stringOfTripleConsonnesDebut+=GenerateSyllables.okTripleConsonnesDebut[iTripleConsonnesDebut];
			stringOfTripleConsonnesDebut+=", ";
			
			if ((iTripleConsonnesDebut + 1) % 20 == 0)
			{
				stringOfTripleConsonnesDebut+="\n";
			}
		}
		if (GenerateSyllables.okTripleConsonnesDebut.length != 0)
		{
			stringOfTripleConsonnesDebut+=GenerateSyllables.okTripleConsonnesDebut[GenerateSyllables.okTripleConsonnesDebut.length-1];
		}
		
		listOfTripleConsonnesDebut.setText(stringOfTripleConsonnesDebut);
		return stringOfTripleConsonnesDebut;
	}
	
	private String fillDoubleConsonnesFin()
	{
		String stringOfDoubleConsonnesFin="";
		for (int iDoubleConsonnesFin = 0; iDoubleConsonnesFin < GenerateSyllables.okDoubleConsonnesEnd.length-1 ; iDoubleConsonnesFin++)
		{
			stringOfDoubleConsonnesFin+=GenerateSyllables.okDoubleConsonnesEnd[iDoubleConsonnesFin];
			stringOfDoubleConsonnesFin+=", ";
			
			if ((iDoubleConsonnesFin + 1) % 20 == 0)
			{
				stringOfDoubleConsonnesFin+="\n";
			}
		}
		if (GenerateSyllables.okDoubleConsonnesEnd.length != 0)
		{
			stringOfDoubleConsonnesFin+=GenerateSyllables.okDoubleConsonnesEnd[GenerateSyllables.okDoubleConsonnesEnd.length-1];
		}
		
		listOfDoubleConsonnesFin.setText(stringOfDoubleConsonnesFin);
		return stringOfDoubleConsonnesFin;
	}
	
	private String fillTripleConsonnesFin()
	{
		String stringOfTripleConsonnesFin="";
		for (int iTripleConsonnesFin = 0; iTripleConsonnesFin < GenerateSyllables.okTripleConsonnesEnd.length-1 ; iTripleConsonnesFin++)
		{
			stringOfTripleConsonnesFin+=GenerateSyllables.okTripleConsonnesEnd[iTripleConsonnesFin];
			stringOfTripleConsonnesFin+=", ";
			
			if ((iTripleConsonnesFin + 1) % 20 == 0)
			{
				stringOfTripleConsonnesFin+="\n";
			}
		}
		if (GenerateSyllables.okTripleConsonnesEnd.length != 0)
		{
			stringOfTripleConsonnesFin+=GenerateSyllables.okTripleConsonnesEnd[GenerateSyllables.okTripleConsonnesEnd.length-1];
		}
		
		listOfTripleConsonnesFin.setText(stringOfTripleConsonnesFin);
		return stringOfTripleConsonnesFin;
	}
	
	/**
	 * @return
	 */
	private String fillBlackList()
	{
		String stringOfBlackList="";
		for (int iBlackList = 0; iBlackList < GenerateNames.blackList.length-1 ; iBlackList++)
		{
			stringOfBlackList+=GenerateNames.blackList[iBlackList];
			stringOfBlackList+=", ";
			
			if ((iBlackList + 1 ) % 20 == 0)
			{
				stringOfBlackList+="\n";
			}
		}
		if (GenerateNames.blackList.length != 0)
		{
			stringOfBlackList+=GenerateNames.blackList[GenerateNames.blackList.length-1];
		}
		
		entryBlackList.setText(stringOfBlackList);
		return stringOfBlackList;
	}
	
	/**
	 * @param c
	 * @param panelx
	 * @param panely
	 * @param labelToAdd
	 * @param textFieldToAdd
	 * @param entryListToAdd
	 * @param radioUsedToAdd
	 * @param resetButtonToAdd
	 */
	private void generateSubPanelForSetOfLetters(GridBagConstraints c, int panelx, int panely, JLabel labelToAdd, JTextArea textFieldToAdd, LettersAndCommaJTextArea entryListToAdd, JRadioButton radioUsedToAdd, JButton resetButtonToAdd)
	{
		JPanel tripleConsonnesMiddlePanel = new JPanel();
		tripleConsonnesMiddlePanel.setLayout(new GridBagLayout());
		
		GridBagConstraints cSub= new GridBagConstraints();
		
		cSub.fill = GridBagConstraints.BOTH;
		cSub.gridx = 0;
		cSub.gridy = 0;
		cSub.gridwidth = 2;
		tripleConsonnesMiddlePanel.add(labelToAdd,cSub);
		
		GridBagConstraints cSub2= new GridBagConstraints();
		
		cSub2.fill = GridBagConstraints.BOTH;
		cSub2.gridx = 0;
		cSub2.gridy = 1;
		cSub2.gridwidth = 1;
		cSub2.gridheight = 1;
		tripleConsonnesMiddlePanel.add(textFieldToAdd,cSub2);
		
		GridBagConstraints cSub3= new GridBagConstraints();
		
		cSub3.fill = GridBagConstraints.BOTH;
		cSub3.gridx = 0;
		cSub3.gridy = 3;
		cSub3.gridwidth = 1;
		cSub3.gridheight = 1;
		tripleConsonnesMiddlePanel.add(entryListToAdd,cSub3);
		
		GridBagConstraints cSub4= new GridBagConstraints();
		
		cSub4.fill = GridBagConstraints.BOTH;
		cSub4.gridx = 1;
		cSub4.gridy = 1;
		cSub4.gridwidth = 1;
		cSub4.gridheight = 1;
		tripleConsonnesMiddlePanel.add(radioUsedToAdd,cSub4);
		
		GridBagConstraints cSub5= new GridBagConstraints();
		
//		cSub.fill = GridBagConstraints.BOTH;
		cSub5.fill = GridBagConstraints.BOTH;
		cSub5.gridx = 1;
		cSub5.gridy = 2;
		cSub5.gridwidth = 1;
		cSub5.gridheight = 1;
		tripleConsonnesMiddlePanel.add(resetButtonToAdd,cSub5);
		
		c.fill = GridBagConstraints.BOTH;
		c.gridx = panelx;
		c.gridy = panely;
		
		getContentPane().add(tripleConsonnesMiddlePanel, c);
	}

	private class UpdateInfoString implements Runnable
	{
		String textToSet;
		
		public void setNewText(String aNewText)
		{
			textToSet = aNewText;
		}
		@Override
		public void run()
		{
			infoArea.setText(textToSet);
		}
		
	}
	@Override
	public void receiveInfoString(String aNewInfo)
	{
		UpdateInfoString doSetInfoString = new UpdateInfoString();
		doSetInfoString.setNewText(aNewInfo);

		SwingUtilities.invokeLater(doSetInfoString);		
	}

	private class UpdateProgressString implements Runnable
	{
		String textToSet;
		
		public void setNewText(String aNewText)
		{
			textToSet = aNewText;
		}
		@Override
		public void run()
		{
			nbOfCombinationsInfo.setText(textToSet);
		}
		
	}

	@Override
	public void receiveProgressString(String progress)
	{
		UpdateProgressString doSetProgressString = new UpdateProgressString();
		doSetProgressString.setNewText(progress);

		SwingUtilities.invokeLater(doSetProgressString);	
	}

	private class UpdateGenerateButton implements Runnable
	{
		boolean isFinished;
		
		public void setIsItFinished(boolean isItFinished)
		{
			isFinished = isItFinished;
		}
		@Override
		public void run()
		{
			 if (isFinished)
	    	 {
	    		 generateButton.setText("Generate");
	    	 }
		}
		
	}
	
	@Override
	public void isFinished(boolean isItFinished)
	{
		UpdateGenerateButton doSetGenerateButton = new UpdateGenerateButton();
		doSetGenerateButton.setIsItFinished(isItFinished);

		SwingUtilities.invokeLater(doSetGenerateButton);

	}
}
